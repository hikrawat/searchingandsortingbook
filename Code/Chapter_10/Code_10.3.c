//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 10.3

#include <stdio.h>

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

int getMax(int *a, int n)
{
    int max = a[0];
    for(int i=1; i<n; i++)
        if(a[i] > max)
            max = a[i];
    return max;
}

void countSort(int *arr, int n, int placeValue)
{
    int finalArr[n]; // FINAL OUTPUT ARRAY
    int count[10] = {0}; // MAX UNIQUE DIGITS ARE ALWAYS 10
    int i;
    
    for (i = 0; i < n; i++)
        count[(arr[i]/placeValue)%10]++;
    
    for (i = 1; i < 10; i++)
        count[i] += count[i-1];
    
    for (i = n - 1; i >= 0; i--)
    {
        finalArr[count[ (arr[i]/placeValue)%10 ] - 1] = arr[i];
        count[ (arr[i]/placeValue)%10 ]--;
    }
    
    for (i = 0; i < n; i++)
        arr[i] = finalArr[i];
}

void radixSort(int arr[], int n)
{
    // FIND MAX NUMBER TO KNOW NUMBER OF DIGITS
    int m = getMax(arr, n);
    
    for (int placeValue = 1; m/placeValue > 0; placeValue *= 10)
        countSort(arr, n, placeValue);
}

int main()
{
    int arr[] = {582, 675, 591, 189, 900, 770};
    
    printf("ORIGINAL ARRAY : "); printArray(arr, 6);
    radixSort(arr, 6);
    printf("SORTED ARRAY : "); printArray(arr, 6);
    
    return 0;
}
