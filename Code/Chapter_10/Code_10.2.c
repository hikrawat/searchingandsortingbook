//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 10.2

#include <stdio.h>

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

void countSort(int *arr, int n, int placeValue)
{
    int finalArr[n]; // FINAL OUTPUT ARRAY
    int count[10] = {0}; // MAX UNIQUE DIGITS ARE ALWAYS 10
    int i;
    
    for (i = 0; i < n; i++)
        count[(arr[i]/placeValue)%10]++;
    
    for (i = 1; i < 10; i++)
        count[i] += count[i-1];
    
    for (i = n - 1; i >= 0; i--)
    {
        finalArr[count[ (arr[i]/placeValue)%10 ] - 1] = arr[i];
        count[ (arr[i]/placeValue)%10 ]--;
    }
    
    for (i = 0; i < n; i++)
        arr[i] = finalArr[i];
}

int main()
{
    int arr[] = {13, 21, 11, 39, 12, 56, 23, 90, 27, 539};
    
    printf("ORIGINAL ARRAY : "); printArray(arr, 10);
    countSort(arr, 10, 10);
    printf("SORTED ARRAY : "); printArray(arr, 10);
    
    return 0;
}
