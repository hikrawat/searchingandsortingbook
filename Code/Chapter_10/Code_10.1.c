//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 10.1

#include <stdio.h>

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

void countingSort(int *arr, int n, int k)
{
    int count[k+1];  int finalArr[n];  int i;
    
    // INITIALIZING COUNT ARRAY WITH 0's
    for(i=0; i<=k; i++)
        count[i] = 0;
    
    // COUNTING NUM OF OCCURRENCES OF EACH NUMBER
    for(i=0; i<n; i++)
        count[arr[i]]++;
    
    for(i=1; i<=k; i++)
        count[i] += count[i-1];
    
    for(i=n-1; i>=0; i--)
    {
        finalArr[count[arr[i]]-1] = arr[i];
        count[arr[i]]--;
    }
    
    // STORING THE ARRAY BACK TO ORIGINAL ARRAY.
    for(i=0; i<n; i++)
        arr[i] = finalArr[i];
}

int main()
{
    int arr[] = {1, 2, 1, 3, 1, 5, 2, 0, 2, 5};
    
    printf("ORIGINAL ARRAY : "); printArray(arr, 10);
    countingSort(arr, 10, 5);
    printf("SORTED ARRAY : "); printArray(arr, 10);
    
    return 0;
}