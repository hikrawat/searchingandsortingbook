//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 4.5

#include <stdio.h>

void printReverse(int *arr, int n)
{
    int arrRev[n], i;
    
    // COPY CONTENTS OF ARRAY arr TO arrRev IN REVERSE ORDER
    for(i=0; i<n; i++)
        arrRev[i] = arr[n-i-1];
    
    printf("Array in reverse order : ");
    for(i=0; i<n; i++)
        printf("%d ", arrRev[i]);
}

int main()
{
    int arr[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    printReverse(arr, 10);
    return 0;
}