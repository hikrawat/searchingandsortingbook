//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 4.3

#include <iostream>
#include <algorithm>
using namespace std;

struct Student
{
    int rollNo;
    char name[25];
    char grade;
};

bool Compare1(struct Student a, struct Student b)
{
    return (a.rollNo < b.rollNo);
}

bool Compare2(struct Student a, struct Student b)
{
    return (a.grade < b.grade);
}

void printArray(Student *arr, int n)
{
    cout<<"\n----------------------------------------\n";
    for(int i=0; i<n; i++)
        cout<<arr[i].rollNo<<" "<<arr[i].name<<" "<<arr[i].grade<<endl;
}

int main()
{
    Student arr[] = { {2, "Ram Chandra", 'C'},
        {3, "Mohan Mehta", 'B'},
        {4, "Moksha Rawat",'A'},
        {1, "Ritambhara",  'C'},
        {5, "Amit Verma",  'B'}};
    
    // SORT USING Compare1 FUNCTION
    std::sort(arr, arr+5, Compare1);
    printArray(arr, 5);
    
    // SORT USING Compare2 FUNCTION
    std::sort(arr, arr+5, Compare2);
    printArray(arr, 5);
}