//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 4.6

#include <stdio.h>

void printReverse(int *arr, int n)
{
    if(n==0){ return; }      // TERMINATING CONDITION.
    printf("%d ", arr[n-1]); // PRINTING LAST ELEMENT.
    printReverse(arr, n-1);    // PRING FIRST n-1 ELEMENTS RECURSIVELY.
}

int main()
{
    int arr[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    printReverse(arr, 10);
    return 0;
}