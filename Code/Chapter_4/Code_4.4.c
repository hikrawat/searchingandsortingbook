//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 4.4

#include <stdio.h>

void printReverse(int *arr, int n)
{
    printf("Array in reverse order : ");
    for(int i=n-1; i>=0; i--)
        printf("%d ", arr[i]);
}

int main()
{
    int arr[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    printReverse(arr, 10);
    return 0;
}