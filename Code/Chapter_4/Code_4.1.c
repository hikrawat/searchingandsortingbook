//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 4.1

#include <stdio.h>

typedef int bool;
#define false 0
#define true 1

bool isArraySorted(int *arr, int n)
{
    // SINGLE ELEMENT ARRAY IS SORTED
    if(n==0 || n==1){ return true; }
    
    for(int i=0; i<n-1; i++)
        if(arr[i] > arr[i+1])  // UNSORTED PAIR
            return false;
    
    return true;
}

int main()
{
    int arr[] = {1, 2, 3, 4, 5, 6};

    if(isArraySorted(arr, 6))
        printf("SORTED");
    else
        printf("UNSORTED");

    return 0;
}