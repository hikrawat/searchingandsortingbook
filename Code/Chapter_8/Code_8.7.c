//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 8.7

#include <stdio.h>

//HELPER FUNCTIONS - START
void swap(double* a, double*b)
{
    double temp = *a;
    *a = *b;
    *b = temp;
}

int partition(double *arr, int low, int high)
{
    int p = low; // Pivot index
    while(low < high)
    {
        while(low <= high && arr[low]<=arr[p]) // MOVING LOW FORWARD
            low++;
        while(low <= high && arr[high]>arr[p])  // MOVING HIGH BACKWARD
            high--;
        if(low < high)
            swap(&arr[low], &arr[high]);
    }
    swap(&arr[p], &arr[high]);
    return high;
}

void quickSort(double *arr, int l, int h)
{
    if(l < h)
    {
        int m = partition(arr, l, h);
        quickSort(arr, l, m-1);
        quickSort(arr, m+1, h);
    }
}

void sortArray(double *arr, int n)
{
    quickSort(arr, 0, n-1);
}
// HELPER FUNCTIONS - END

int minPlatformsRequired(double arivl[], double deprt[], int noOfTrains)
{
    // IMPLEMENT THIS FUNCTION TO SORT THE ARRAY
    sortArray(arivl, noOfTrains);
    sortArray(deprt, noOfTrains);
    
    int maxPlatforms = 0;
    int platformsRequired = 0;
    int i = 0, j = 0;
    
    // LOGIC SIMILAR TO MERGING
    while (i < noOfTrains && j < noOfTrains)
    {
        if (arivl[i] < deprt[j])
        {
            // NEW TRAIN ARRIVED.
            platformsRequired++;
            i++;
            if (platformsRequired > maxPlatforms)
                maxPlatforms = platformsRequired;
        }
        else
        {
            // TRAIN LEFT PLATFORM.
            platformsRequired--;
            j++;
        }
    }
    return maxPlatforms;
}

int main()
{
    double arivl[] = {9.00,  9.35,  9.45, 11.00, 14.30, 18.00};
    double deprt[] = {9.15, 11.45, 11.05, 12.00, 18.15, 19.00};
    
    printf("MIN PLATFORMS REQUIRED : %d", minPlatformsRequired(arivl, deprt, 6));
    
    return 0;
}