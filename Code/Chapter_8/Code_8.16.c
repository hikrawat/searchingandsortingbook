//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 8.16

#include <stdio.h>

//HELPER FUNCTIONS - START
void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}
// HELPER FUNCTIONS - END

// SPLIT ARRAY IN ALTERNATE ELEMENTS AND RETURN THE MIDDLE INDEX
int splitAlternate(int *arr, int left, int right)
{
    int mid = left + (right-left)/2;
    int i;

    // ARRAYS TO HOLD LEFT AND RIGHT PARTS
    int leftPart[mid-left+1];
    int rightPart[right-mid];
    
    //SEPARATING LEFT AND RIGHT PARTS
    for(i = 0; i <= mid - left; i++)
        leftPart[i] = arr[left + i*2];

    for(i = 0; i < right - mid; i++)
        rightPart[i] = arr[left + i*2+1];

    // PUTTING THE TWO PARTS BACK IN ORIGINAL ARRAY
    for(i = 0; i <= mid - left; i++)
        arr[left+i] = leftPart[i];

    for(int j = 0; j < right - mid; j++)
        arr[left+i + j] = rightPart[j];

    return mid;
}

void generateWorstCase(int *arr, int left, int right)
{
    if(left < right-1)
    {
        int mid = splitAlternate(arr, left, right);
        generateWorstCase(arr, left, mid);
        generateWorstCase(arr, mid + 1, right);
    }
}

int main()
{
    int arr[] = {1, 2, 3, 4, 5, 6, 7, 8};
    
    printf("\nORIGINAL ARRAY :"); printArray(arr, 8);
    generateWorstCase(arr, 0, 7);
    printf("FINAL ARRAY   : "); printArray(arr, 8);
    
    return 0;
}
