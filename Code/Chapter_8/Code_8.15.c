//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 8.15

#include <stdio.h>

void generateWorstCase(int *arr, int left, int right)
{
    if(left < right-1)
    {
        int mid = splitAlternate(arr, left, right);
        generateWorstCase(arr, left, mid);
        generateWorstCase(arr, mid + 1, right);
    }
}