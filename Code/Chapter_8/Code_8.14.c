//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 8.14

#include <stdio.h>

//HELPER FUNCTIONS - START
void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}
// HELPER FUNCTIONS - END

int getInvCount(int *arr, int n)
{
    int count = 0;
    int i, j;
    for(i=0; i<n-1; i++)
        for(j=i+1; j<n; j++)
            if(arr[i] > arr[j])
                count++;
    return count;
}

int main()
{
    int arr[] = {8, 12, 3, 10, 15};
    
    printf("\nORIGINAL ARRAY :"); printArray(arr, 5);
    printf("INVERSION COUNT : %d",getInvCount(arr, 5));
    
    return 0;
}
