//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 8.3

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int data;
    struct node* next;
} Node;

Node* addNode(int v)
{
    Node * temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->next = NULL;
    return temp;
}

void printfList(Node* head)
{
    while(head != NULL)
    {
        printf("%d ", head->data);
        head = head->next;
    }
}

Node* mergeLists(Node* h1, Node* h2)
{
    if(h1 == NULL) { return h2; }
    if(h2 == NULL) { return h1; }

    Node* head = NULL; // HEAD OF MERGED LIST
    Node* tail = NULL; // TAIL OF MERGED LIST. NEEDED TO APPEAND NODES.

    // SETTING THE head
    if(h1->data < h2->data)
    {
        head = tail = h1;
        h1 = h1->next;
    }
    else
    {
        head = tail = h2;
        h2 = h2->next;
    }

    while(h1 != NULL && h2 !=NULL)
    {
        if(h1->data < h2->data)
        {
            // ADDING FROM 1st LIST
            tail->next = h1;
            tail = h1;
            h1 = h1->next;
        }
        else
        {
            // ADDING FROM 2nd LIST
            tail->next = h2;
            tail = h2;
            h2 = h2->next;
        }
    }
    
    // ADDING REMAINING ELEMENTS
    if(h1 == NULL)
        tail->next = h2;
    else
        tail->next = h1;
    
    return head;
}

int main()
{
    Node *h1 = addNode(1);
    h1->next = addNode(6);
    h1->next->next = addNode(8);
    h1->next->next->next = addNode(14);
    h1->next->next->next->next = addNode(20);
    h1->next->next->next->next->next = addNode(25);
    
    Node *h2 = addNode(2);
    h2->next = addNode(3);
    h2->next->next = addNode(18);
    h2->next->next->next = addNode(40);
    h2->next->next->next->next = addNode(42);
    
    printf("ORIGINAL LISTS :");
    printf("\n"); printfList(h1);
    printf("\n"); printfList(h2);

    Node *h3 = mergeLists(h1, h2);
    printf("\nMERGED List :");
    printf("\n"); printfList(h3);
    
    return 0;
}

