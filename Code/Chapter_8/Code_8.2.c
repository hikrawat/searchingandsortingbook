//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 8.2

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int data;
    struct node* next;
} Node;

Node* addNode(int v)
{
    Node * temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->next = NULL;
    return temp;
}

void printfList(Node* head)
{
    while(head != NULL)
    {
        printf("%d ", head->data);
        head = head->next;
    }
}

Node* mergeListRec(Node* h1, Node* h2)
{
    if(h1 == NULL) { return h2; }
    if(h2 == NULL) { return h1; }
    
    Node* head = NULL;             // POINTER TO MERGED LIST
    if(h1->data < h2->data)
    {
        head = h1;
        head->next = mergeListRec(h1->next, h2);  // MERGE REMAINING
    }
    else
    {
        head = h2;
        head->next = mergeListRec(h1, h2->next);  // MERGE REMAINING
    }
    return head;
}

int main()
{
    Node *h1 = addNode(1);
    h1->next = addNode(6);
    h1->next->next = addNode(8);
    h1->next->next->next = addNode(14);
    h1->next->next->next->next = addNode(20);
    h1->next->next->next->next->next = addNode(25);
    
    Node *h2 = addNode(2);
    h2->next = addNode(3);
    h2->next->next = addNode(18);
    h2->next->next->next = addNode(40);
    h2->next->next->next->next = addNode(42);
    
    printf("ORIGINAL LISTS :");
    printf("\n"); printfList(h1);
    printf("\n"); printfList(h2);

    Node *h3 = mergeListRec(h1, h2);
    printf("\nMERGED List :");
    printf("\n"); printfList(h3);
    
    return 0;
}
