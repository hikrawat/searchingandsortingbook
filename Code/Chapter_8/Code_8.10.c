//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 8.10

#include <stdio.h>
#define THRESHOLD 3

//HELPER FUNCTIONS - START
void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

void insertionSort(int *a, int low, int high)
{
    for(int i=low+1; i<=high; i++)
    {
        int j, temp = a[i];
        for(j=i-1; j>=low && a[j] > temp; j--)
            a[j+1] = a[j];
        a[j+1] = temp;
    }
}
// HELPER FUNCTIONS - END

void mergeWithinArray(int *arr, int low, int mid, int high)
{
    if(low>=high || low==mid+1 || mid==high) { return; }

    int m = mid - low + 1; // firstArr
    int n = high - mid; // secondArr

    // Auxiliary arrays to store two sub-arrays
    int firstArr[m];
    int secondArr[n];

    // Copy the two sub-arrays to auxiliary arrays
    for (int i = low; i <= mid; i++)
        firstArr[i-low] = arr[i];

    for (int i = mid+1; i <= high; i++)
        secondArr[i-mid-1] = arr[i];
    
    // Merge firstArr and secondArr into arr[low...high]
    int a = 0;
    int b = 0;
    int c = low;

    while(a<m && b<n)
    {
        if(firstArr[a] <= secondArr[b])
            arr[c++] = firstArr[a++];
        else
            arr[c++] = secondArr[b++];
    }

    // Copy remaining elements
    while (a < m)
        arr[c++] = firstArr[a++];

    while (b < n)
        arr[c++] = secondArr[b++];
}

void mergeSort(int *arr, int low, int high)
{
    if (high > low)
    {
        if(high-low+1 <= THRESHOLD)
        {
            insertionSort(arr, low, high);
        }
        else
        {
            int mid = (low + high) / 2;
            mergeSort(arr, low, mid);
            mergeSort(arr, mid+1, high);
            mergeWithinArray(arr, low, mid, high);
        }
    }
}

int main()
{
    int arr[] = {9, 0, 1, 6, 3, 2, 4, 7, 5, 8};
    
    printf("\nORIGINAL ARRAY :"); printArray(arr, 10);
    mergeSort(arr, 0, 6);
    printf("SORTED ARRAY   :"); printArray(arr, 10);
    
    return 0;
}