//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 8.12

#include <stdio.h>
#include <stdlib.h>

#define false 0
#define true 1
typedef int bool;

//HELPER FUNCTIONS - START
typedef struct node
{
    int data;
    struct node* next;
} Node;

void swapNodeValues(Node *a, Node *b)
{
    int temp = a->data; a->data = b->data; b->data = temp;
}

Node* addNode(int v)
{
    Node * temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->next = NULL;
    return temp;
}

void printfList(Node* head)
{
    while(head != NULL)
    {
        printf("%d ", head->data);
        head = head->next;
    }
}
// HELPER FUNCTIONS - END

void divideInTwo(Node* head, Node** firstHalf, Node** secondHalf)
{
    *firstHalf = head;        // FIRST HALF IS ALWAYS AT HEAD
    
    if (head==NULL || head->next==NULL)
        *secondHalf = NULL;
    else
    {
        Node* slow = head; Node* fast = head->next; // SLOW & FAST POINTERS
        
        while(fast != NULL)
        {
            fast = fast->next;
            if(fast != NULL)
            {
                slow = slow->next;
                fast = fast->next;
            }
        }
        
        *secondHalf = slow->next;
        slow->next = NULL;
    }
}

int main()
{
    Node *h = addNode(9);
    h->next = addNode(6);
    h->next->next = addNode(1);
    h->next->next->next = addNode(4);
    h->next->next->next->next = addNode(2);
    h->next->next->next->next->next = addNode(5);
    
    printf("ORIGINAL LIST : "); printfList(h);
    
    Node *first, *second;
    divideInTwo(h, &first, &second);
    printf("\nDIVIDED LISTS   : ");
    printfList(first);
    printf("\n");
    printfList(second);
    
    return 0;
}