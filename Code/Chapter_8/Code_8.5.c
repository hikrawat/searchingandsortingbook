//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 8.5

#include <stdio.h>

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

int findMedian(int* a, int m, int*b, int n)
{
    // BOUNDARY CONDITIONS
    if(m == 0 && n == 0)
        return 0;
    if(m == 0)
        return b[n/2];
    if(n == 0)
        return a[m/2];
    
    int i = 0;
    int j = 0;
    
    int mid = (m+n)/2;	// MEDIAN INDEX
    int count = 0;
    int retValue = 0;
    while(count < mid && i<m && j<n)
    {
        if( a[i] < b[j] ){
            retValue = a[i]; i++;
        }else{
            retValue = b[j]; j++;
        }
        count++;
    }
    
    if(count < mid)
    {
        if(i>=m){
            while(count < mid){
                retValue = b[j]; j++; count++;
            }
        }else{
            while(count < mid){
                retValue = a[i]; i++; count++;
            }
        }
    }
    return retValue;
}

int main()
{
    int a[] = {1, 3, 5, 6, 7};
    int b[] = {2, 4, 6, 8, 10, 12, 14};
    
    printf("ORIGINAL ARRAYS :"); printArray(a, 5); printArray(b, 7);

    printf("MEDIAN : %d", findMedian(a, 5, b, 7));
    
    return 0;
}
