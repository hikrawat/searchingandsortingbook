//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 8.13

#include <stdio.h>
#include <stdlib.h>

#define false 0
#define true 1
typedef int bool;

//HELPER FUNCTIONS - START
typedef struct node
{
    int data;
    struct node* next;
} Node;

void swapNodeValues(Node *a, Node *b)
{
    int temp = a->data; a->data = b->data; b->data = temp;
}

Node* addNode(int v)
{
    Node * temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->next = NULL;
    return temp;
}

void printfList(Node* head)
{
    while(head != NULL)
    {
        printf("%d ", head->data);
        head = head->next;
    }
}

Node* mergeLists(Node* h1, Node* h2)
{
    if(h1 == NULL) { return h2; }
    if(h2 == NULL) { return h1; }
    
    Node* head = NULL; // HEAD OF MERGED LIST
    Node* tail = NULL; // TAIL OF MERGED LIST. NEEDED TO APPEAND NODES.
    
    // SETTING THE head
    if(h1->data < h2->data)
    {
        head = tail = h1;
        h1 = h1->next;
    }
    else
    {
        head = tail = h2;
        h2 = h2->next;
    }
    
    while(h1 != NULL && h2 !=NULL)
    {
        if(h1->data < h2->data)
        {
            // ADDING FROM 1st LIST
            tail->next = h1;
            tail = h1;
            h1 = h1->next;
        }
        else
        {
            // ADDING FROM 2nd LIST
            tail->next = h2;
            tail = h2;
            h2 = h2->next;
        }
    }
    
    // ADDING REMAINING ELEMENTS
    if(h1 == NULL)
        tail->next = h2;
    else
        tail->next = h1;
    
    return head;
}
// HELPER FUNCTIONS - END

void divideInTwo(Node* head, Node** firstHalf, Node** secondHalf)
{
    *firstHalf = head;        // FIRST HALF IS ALWAYS AT HEAD
    
    if (head==NULL || head->next==NULL)
        *secondHalf = NULL;
    else
    {
        Node* slow = head; Node* fast = head->next; // SLOW & FAST POINTERS
        
        while(fast != NULL)
        {
            fast = fast->next;
            if(fast != NULL)
            {
                slow = slow->next;
                fast = fast->next;
            }
        }
        
        *secondHalf = slow->next;
        slow->next = NULL;
    }
}

void mergeSort(Node** headPtr)
{
    if(headPtr == NULL || *headPtr == NULL || (*headPtr)->next == NULL)
        return;
    
    Node* head = *headPtr;
    Node* a = NULL;
    Node* b = NULL;
    
    divideInTwo(head, &a, &b);
    
    mergeSort(&a);
    mergeSort(&b);
    
    *headPtr = mergeLists(a, b);
}

int main()
{
    Node *h = addNode(9);
    h->next = addNode(6);
    h->next->next = addNode(1);
    h->next->next->next = addNode(4);
    h->next->next->next->next = addNode(2);
    h->next->next->next->next->next = addNode(5);
    
    printf("ORIGINAL LIST : "); printfList(h);
    mergeSort(&h);
    printf("\nSORTED LIST  : "); printfList(h);

    
    return 0;
}
