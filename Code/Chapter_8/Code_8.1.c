//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 8.1

#include <stdio.h>

// HELPER FUNCTIONS - START
void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}
// HELPER FUNCTIONS - END

void merge(int *a, int m, int *b, int n, int *c)
{
    int i=0, j=0, k=0; // INDEX VARIABLES FOR a, b AND c
    
    // MERGING TWO ARRAYS
    while(i<m && j<n){
        if(a[i] < b[j]){
            c[k] = a[i]; i++;
        }else{
            c[k] = b[j]; j++;
        }
        k++;
    }
    
    // ELEMENTS LEFT IN FIRST ARRAY
    if(i<m)
        while(i<m){
            c[k] = a[i]; i++; k++;
        }
    // ELEMENTS LEFT IN SECOND ARRAY
    else
        while(j<n){
            c[k] = b[j]; j++; k++;
        }
}

int main()
{
    int a[] = {1, 4, 5, 9};
    int b[] = {0, 2, 3, 7, 15, 29};
    int c[10];
    
    printf("ORIGINAL ARRAYS :"); printArray(a, 4); printArray(b, 6);
    merge(a, 4, b, 6, c);
    printf("MERGED ARRAYS   :"); printArray(a, 4); printArray(b, 6); printArray(c, 10);
    
    return 0;
}
