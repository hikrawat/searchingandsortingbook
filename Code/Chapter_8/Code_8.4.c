//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 8.4

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int data;
    struct node* next;
} Node;

Node* addNode(int v)
{
    Node * temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->next = NULL;
    return temp;
}

void printfList(Node* head)
{
    while(head != NULL)
    {
        printf("%d ", head->data);
        head = head->next;
    }
}

Node* merge( Node *h1, Node *h2)
{
    if(h1 == NULL) { return h2; }
    if(h2 == NULL) { return h1; }

    Node *head = h1;
    while (h1 != NULL && h2 != NULL)
    {
        Node* temp1 = h1->next;
        h1->next = h2;

        Node* temp2 = h2->next;
        if(temp1 != NULL)
            h2->next = temp1;

        h1 = temp1;
        h2 = temp2;
    }
    return head;
}

int main()
{
    Node *h1 = addNode(5);
    h1->next = addNode(3);
    h1->next->next = addNode(7);
    h1->next->next->next = addNode(1);
    
    Node *h2 = addNode(0);
    h2->next = addNode(2);
    h2->next->next = addNode(8);
    h2->next->next->next = addNode(4);
    h2->next->next->next->next = addNode(6);
    
    printf("ORIGINAL LISTS :");
    printf("\n"); printfList(h1);
    printf("\n"); printfList(h2);

    Node *h3 = merge(h1, h2);
    printf("\nMERGED List :");
    printf("\n"); printfList(h3);
    
    return 0;
}