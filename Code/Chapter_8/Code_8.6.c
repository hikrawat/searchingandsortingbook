//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 8.6

#include <stdio.h>

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

void printUnion(int* a, int m, int* b, int n)
{
    int i = 0, j = 0;
    while (i < m && j < n)
    {
        if(a[i] < b[j]){
            printf("%d ", a[i++]);
            while(i<m && a[i-1]==a[i]){ i++; }
        }else if(a[i] > b[j]){
            printf("%d ", b[j++]);
            while(j<n && b[j-1]==b[j]){ j++; }
        }else{
            printf("%d ", a[i]);
            i++; j++;
            while(i<m && a[i-1]==a[i]){ i++; }
            while(j<n && b[j-1]==b[j]){ j++; }
        }
    }
    
    // PRINT ELEMENTS FROM NON-EMPTY ARRAY
    while(i < m)
        printf("%d ", a[i++]);
    
    while(j < n)
        printf(" %d ", b[j++]);
}

int main()
{
    int a[] = {1, 3, 5, 6, 7};
    int b[] = {2, 4, 6, 8, 10, 12, 14};
    
    printf("ORIGINAL ARRAYS :"); printArray(a, 5); printArray(b, 7);

    printUnion(a, 5, b, 7);
    
    return 0;
}
