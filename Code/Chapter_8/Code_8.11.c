//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 8.11

#include <stdio.h>
#define THRESHOLD 3

//HELPER FUNCTIONS - START
void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}
// HELPER FUNCTIONS - END

void mergeWithinArray(int *arr, int low, int mid, int high)
{
    if(low>=high || low==mid+1 || mid==high) { return; }

    int m = mid - low + 1; // firstArr
    int n = high - mid; // secondArr

    // Auxiliary arrays to store two sub-arrays
    int firstArr[m];
    int secondArr[n];

    // Copy the two sub-arrays to auxiliary arrays
    for (int i = low; i <= mid; i++)
        firstArr[i-low] = arr[i];

    for (int i = mid+1; i <= high; i++)
        secondArr[i-mid-1] = arr[i];
    
    // Merge firstArr and secondArr into arr[low...high]
    int a = 0;
    int b = 0;
    int c = low;

    while(a<m && b<n)
    {
        if(firstArr[a] <= secondArr[b])
            arr[c++] = firstArr[a++];
        else
            arr[c++] = secondArr[b++];
    }

    // Copy remaining elements
    while (a < m)
        arr[c++] = firstArr[a++];

    while (b < n)
        arr[c++] = secondArr[b++];
}

void bottomUpMergeSort(int *arr, int n)
{
    int runLength = 1;
    while(runLength<n)
    {
        for(int i=0; i<n; i+=runLength*2)
        {
            int low = i;
            int mid = (low+runLength)<n ? (low+runLength)-1 : n-1;
            int high = (mid+runLength)<n ? (mid+runLength) : n-1;
            mergeWithinArray(arr, low, mid, high);
        }
        runLength *= 2;
    }
}

int main()
{
    int arr[] = {9, 0, 1, 6, 3, 2, 4, 7, 5, 8};
    
    printf("\nORIGINAL ARRAY :"); printArray(arr, 10);
    bottomUpMergeSort(arr, 10);
    printf("SORTED ARRAY   :"); printArray(arr, 10);
    
    return 0;
}