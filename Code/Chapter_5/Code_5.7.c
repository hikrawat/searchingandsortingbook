//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 5.7

#include <stdio.h>
#include <stdlib.h>

typedef int bool;
#define false 0
#define true 1

// HELPER FUNCTIONS - START
void swap(int* a, int*b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}
void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}
// HELPER FUNCTIONS - END

void oddEvenSort(int *arr, int n)
{
    bool isSwapped = true;
    int i;
    
    while(isSwapped)
    {
        isSwapped = false;
        
        // ODD-LOOP
        for(i=1; i<=n-2; i=i+2)
        {
            if(arr[i] > arr[i+1])
            {
                swap(&arr[i], &arr[i+1]);
                isSwapped = true;
            }
        }
        
        // EVEN LOOP
        for(i=0; i<=n-2; i=i+2)
        {
            if (arr[i] > arr[i+1])
            {
                swap(&arr[i], &arr[i+1]);
                isSwapped = true;
            }
        }
    }
}

int main()
{
    int arr[] = {9, 0, 1, 6, 3, 2, 4, 7, 5, 8};
    
    printf("\nstudent Knowedge :"); printArray(arr, 10);
    oddEvenSort(arr, 10);
    printf("Open Positions   :"); printArray(arr, 10);
}