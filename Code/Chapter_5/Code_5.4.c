//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 5.4

#include <stdio.h>
#include <stdlib.h>

void swap(int* a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

void selectionSort(int *arr, int n)
{
    for(int i=n-1; i>=0 ; i--)
    {
        int max = i; 		// HOLD INDEX OF LARGEST ELEMENT IN CURRENT PASS
        for(int j=0 ; j<i; j++)
            if(arr[j] > arr[max]) { max = j; }

        if(max != i)
            swap(&arr[i], &arr[max]);
    }
}

int main()
{
    int arr[] = { 5, 6, 1, 3, 2, 9, 0, 8, 4, 7};
    
    printf("Original Array :"); printArray(arr, 10);
    selectionSort(arr, 10);
    printf("Sorted Array   :"); printArray(arr, 10);
}