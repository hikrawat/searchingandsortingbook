//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 5.6

#include <stdio.h>
#include <stdlib.h>

typedef int bool;
#define false 0
#define true 1

// HELPER FUNCTIONS - START
void swap(int* a, int*b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}
void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}
// HELPER FUNCTIONS - END

void cocktailSort(int *arr, int n)
{
    int startPos=0, endPos=n-1, i;
    
    while( startPos < endPos )
    {
        // FORWARD LOOP
        bool isSwapped = false;
        for(i = startPos; i < endPos; i++)
        {
            if(arr[i] > arr[i+1])
            {
                swap(&arr[i], &arr[i+1]);
                isSwapped = true;
            }
        }
        
        endPos--;
        if(!isSwapped){ break; }
        
        // BACKWARD LOOP
        isSwapped = false;
        for(i = endPos-1; i >= startPos; i--)
        {
            if(arr[i] > arr[i+1])
            {
                swap(&arr[i], &arr[i+1]);
                isSwapped = true;
            }
        }
        
        startPos++;
        if(!isSwapped){ break; }
        break;
    }
}

int main()
{
    int arr[] = {9, 0, 1, 6, 3, 2, 4, 7, 5, 8};
    
    printf("\nstudent Knowedge :"); printArray(arr, 10);
    cocktailSort(arr, 10);
    printf("Open Positions   :"); printArray(arr, 10);
}