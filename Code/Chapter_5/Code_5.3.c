//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 5.3

#include <stdio.h>
#include <stdlib.h>

typedef int bool;
#define true 1
#define false 0

typedef struct node
{
    int data;
    struct node* next;
} Node;

void swapNodeValues(Node *a, Node *b)
{
    int temp = a->data; a->data = b->data; b->data = temp;
}

void bubbleSort(Node *head)
{
    Node *end = NULL;
    bool isSwapped = true;
    while(isSwapped)
    {
        isSwapped = false;
        Node *temp = head;
        
        while (temp->next != end)
        {
            if (temp->data > temp->next->data)
            {
                swapNodeValues(temp, temp->next);
                isSwapped = true;
            }
            temp = temp->next;
        }
        end = temp;  // REDUCE SIZE
    }
}

Node* addNode(int v)
{
    Node * temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->next = NULL;
    return temp;
}

void printfList(Node* head)
{
    while(head != NULL)
    {
        printf("%d ", head->data);
        head = head->next;
    }
}

int main()
{
    Node *h = addNode(9);
    h->next = addNode(6);
    h->next->next = addNode(1);
    h->next->next->next = addNode(4);
    h->next->next->next->next = addNode(2);
    h->next->next->next->next->next = addNode(5);
    
    printf("ORIGINAL LIST : "); printfList(h);
    bubbleSort(h);
    printf("\nSORTED LIST   : "); printfList(h);
    
    return 0;
}
