//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 5.2

#include <stdio.h>

// HELPER FUNCTIONS - START
void swap(int* a, int*b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}
void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}
// HELPER FUNCTIONS - END

void bubbleSortOptimized(int *arr, int n)
{
    for(int passNo=0; passNo<n-1; passNo++)
    {
        int swapDone = 0;    // FLAG TO CHECK IF SWAP HAPPENS
        for(int j=0; j<n-passNo-1; j++)
        {
            if(arr[j] > arr[j+1])
            {
                swap(&arr[j], &arr[j+1]);
                swapDone = 1;    // ELEMENTS ARE SWAPPED.
            }
        }
        // IF NO SWAP IN THE PASS. THEN ARRAY IS ALREADY SORTED.
        if(!swapDone)
            return;
    }
}

int main()
{
    int arr[] = {12, 2, 4, 6, 7, 21, 9, 13};
    printf("Original Array :"); printArray(arr, 8);
    bubbleSortOptimized(arr, 8);
    printf("Array After Sorting :"); printArray(arr, 8);
    return 0;
}
