//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 5.1

#include <stdio.h>

// HELPER FUNCTIONS - START
void swap(int* a, int*b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}
void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}
// HELPER FUNCTIONS - END

void bubbleSort(int *arr, int n)
{
    for(int passNo=0; passNo<n-1; passNo++)
    {
        for(int j=0; j<n-passNo-1; j++)
        {
            if(arr[j] > arr[j+1])
                swap(&arr[j], &arr[j+1]);
        }
    }
}

int main()
{
    int arr[] = {12, 2, 4, 6, 7, 21, 9, 13};
    printf("Original Array :"); printArray(arr, 8);
    bubbleSort(arr, 8);
    printf("Array After Sorting :"); printArray(arr, 8);
    return 0;
}