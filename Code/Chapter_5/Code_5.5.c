//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 5.5

#include <stdio.h>
#include <stdlib.h>

// HELPER FUNCTIONS - START
void swap(int* a, int*b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}
void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

void bubbleSort(int *arr, int n)
{
    for(int passNo=0; passNo<n-1; passNo++)
    {
        for(int j=0; j<n-passNo-1; j++)
        {
            if(arr[j] > arr[j+1])
                swap(&arr[j], &arr[j+1]);
        }
    }
}
// HELPER FUNCTIONS - END

void chekHire(int *arr1, int *arr2, int n)
{
    bubbleSort(arr1, n);
    bubbleSort(arr2, n);
    
    for(int i=0; i<n; i++)
    {
        if(arr1[i] < arr2[i])
        {
            printf("NOT HIRE"); return;
        }
    }
    printf("HIRE");
}

int main()
{
    int studentKnow[] = {13, 46, 34, 84, 49};
    int openPos[] = {10, 39, 48, 79, 22};
    
    chekHire(studen