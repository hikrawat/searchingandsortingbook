//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 2.2

#include <stdio.h>

int binarySearch(int *arr, int n, int x)
{
    if(arr == NULL)
        return -1; // SEE FOOTNOTE.
    
    int l=0, h=n-1;
    while(l<=h)
    {
        int mid = (l+h)/2;
        if(arr[mid] == x)
            return mid;         // ELEMENT FOUND.
        else if(arr[mid] > x)
            h = mid - 1;        // ELEMENT AT MID > X. SEARCH FIRST HALF
        else
            l = mid + 1;        // ELEMENT AT MID < X. SEARCH SECOND HALF
    }
    // ARRAY EXHAUSTED. ELEMENT NOT FOUND.
    return -1;
}

int main()
{
    int arr[] = {1, 3, 4, 6, 7, 8, 9, 12, 15, 20};
    int d;
    printf("Enter value to be searched :");scanf("%d", &d);
    int pos = binarySearch(arr, 10, d);
    if( pos == -1)
        printf("%d NOT FOUND IN ARRAY", d);
    else
        printf("%d FOUND AT POS %d", d, pos);
    
    return 0;
}