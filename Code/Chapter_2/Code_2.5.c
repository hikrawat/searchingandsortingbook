//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 2.5

#include <stdio.h>

int firstOcc(int *arr, int l, int h, int x)
{
    if(h >= l)
    {
        int mid = (l + h)/2;
        if( ( mid == 0 || x > arr[mid-1]) && arr[mid] == x)
            return mid;
        else if(x > arr[mid])
            return firstOcc(arr, (mid + 1), h, x);
        else
            return firstOcc(arr, l, (mid -1), x);
    }
    return -1;
}

int main()
{
    int arr[] = {1, 3, 3, 4, 4 ,5, 5, 5, 7, 8, 8};	int x = 8;
    
    printf("%d APPEARS FIRST AT POS :%d", x, firstOcc(arr, 0, 10, x));

    return 0;
}