//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 2.14

#include <stdio.h>

int binarySearch(int *arr, int l, int h, int x)
{
    if(l>h)
        return 0;           // ELEMENT NOT FOUND.
    
    int mid = (l+h)/2;
    if(arr[mid] == x)
        return 1;           // ELEMENT FOUND AT POSITION mid.
    else if(arr[mid] > x) // ELEMENT AT MID > X. SEARCH IN FIRST HALF
        return binarySearch(arr, l, mid-1, x);
    else                 // ELEMENT AT MID < X. SEARCH IN SECOND HALF
        return binarySearch(arr, mid+1, h, x);
}
int min(int a, int b)
{
    return a<b?a:b;
}

int exponentialSearch(int *arr, int n, int x)
{
    if(arr[0] == x){ return 0; }
    
    // FINDING RANGE FOR BINARY SEARCH
    int i = 1;
    while(i < n && arr[i] < x)
        i = i*2;
    
    if(arr[i] == x)
        return i;

    // CALL BINARY SEARCH FOR RANGE
    return binarySearch(arr, i/2, min(i, n), x);
}

int main()
{
    int arr[] = {1, 3, 4, 5, 8, 10, 12, 14, 19, 25};
    
    int d;
    printf("Enter value to search :"); scanf("%d",&d);
    
    if(exponentialSearch(arr, 11, d))
        printf("%c FOUND", d);
    else
        printf("%c NOT FOUND IN ARRAY", d);
}