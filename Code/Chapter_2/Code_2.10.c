//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 2.10

#include <stdio.h>
#define N 4

int searchMatrix(int arr[N][N], int data)
{
    for(int i=0; i<N; i++)
        for(int j=0; j<N; j++)
            if(arr[i][j] == data)
            {
                printf("FOUND AT POSITION : (%d, %d)", i, j);
                return 1;            // SUCCESS. FOUND.
            }
    printf("NOT FOUND");
    return 0;                  // FAILURE. NOT-FOUND.
}


int main()
{
    int arr[N][N] = {{10, 20, 30, 40},
        {15, 25, 35, 45},
        {27, 29, 37, 48},
        {32, 33, 39, 50}};

    int d;
    printf("Enter value to be searched :"); scanf("%d", &d);
    
    searchMatrix(arr, d);
    
    return 0;
}