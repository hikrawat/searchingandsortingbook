//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 2.3

#include <stdio.h>

// n: Total number of elements, l: low index, h: high index
int findMaximum(int *arr, int n, int low, int high)
{
    // IF ARRAY IS NOT ROTATED
    if(high < low)
        return arr[n-1];

    int mid = (low + high)/2;

    // CHECK IF ELEMENT AT mid IS MAX
    if(mid<n-1 && arr[mid+1] < arr[mid])
        return arr[mid];

    // DECIDE TO GO LEFT OR RIGHT
    if(arr[high] > arr[mid])
        return findMaximum(arr, n, low, mid-1);
    else
        return findMaximum(arr, n, mid+1, high);
}

int main()
{
    int arr[] = {5, 6, 7, 1, 2, 3};
    printf("Maximum Element in the array is: %d",findMaximum(arr, 6, 0, 5));

    return 0;
}