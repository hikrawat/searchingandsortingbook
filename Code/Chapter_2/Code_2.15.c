//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 2.15

#include <stdio.h>

int getPeakElement(int *arr, int l, int h, int n)
{
    int mid = l + (h - l)/2;  // SAME AS (l+h)/2
    
    // IF arr[mid] IS A PEAK
    if(( mid==0   || arr[mid-1]<=arr[mid]) &&
       ( mid==n-1 || arr[mid+1]<=arr[mid]))
        return arr[mid];
    
    if(mid>0 && arr[mid-1]>arr[mid])
        return getPeakElement(arr, l, (mid -1), n);
    else
        return getPeakElement(arr, (mid + 1), h, n);
}

int main()
{
    int arr[] = {3, 5, 9, 6, 2};
    
    printf("Peak Element %d",getPeakElement(arr, 0, 4, 5));
}