//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 2.6

#include <stdio.h>

int lastOcc(int *arr, int n, int l, int h, int x)
{
    if(h >= l)
    {
        int mid = (l + h)/2;
        if( ( mid == n-1 || x < arr[mid+1]) && arr[mid] == x )
            return mid;
        else if(x < arr[mid])
            return lastOcc(arr, n, l, (mid -1), x);
        else
            return lastOcc(arr, n, (mid + 1), h, x);
    }
    return -1;
}

int main()
{
    int arr[] = {1, 3, 3, 4, 4 ,5, 5, 5, 7, 8, 8};	int x = 8;
    
    printf("%d APPEARS LAST AT POS :%d", x, lastOcc(arr, 11, 0, 10, x));

    return 0;
}
