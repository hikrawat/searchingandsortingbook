//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 2.1

#include <stdio.h>

int binarySearch(int *arr, int l, int h, int x)
{
    if(l>h)
        return 0;           // ELEMENT NOT FOUND.
    
    int mid = (l+h)/2;
    if(arr[mid] == x)
        return 1;           // ELEMENT FOUND AT POSITION mid.
    else if(arr[mid] > x) // ELEMENT AT MID > X. SEARCH IN FIRST HALF
        return binarySearch(arr, l, mid-1, x);
    else                 // ELEMENT AT MID < X. SEARCH IN SECOND HALF
        return binarySearch(arr, mid+1, h, x);
}

int main()
{
    int arr[] = {1, 3, 4, 6, 7, 8, 9, 12, 15, 20};
    int d;
    printf("Enter value to be searched :");scanf("%d", &d);
    
    if(binarySearch(arr, 0, 9, d))
        printf("%d FOUND IN ARRAY", d);
    else
        printf("%d IS NOT FOUND IN THE ARRAY", d);
    
    return 0;
}