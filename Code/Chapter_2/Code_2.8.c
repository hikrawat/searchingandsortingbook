//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 2.8

#include <stdio.h>

// HELPER FUNCTIONS - START
void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

int binarySearch(int *arr, int l, int h, int x)
{
    if(l>h)
        return 0;           // ELEMENT NOT FOUND.
    
    int mid = (l+h)/2;
    if(arr[mid] == x)
        return 1;           // ELEMENT FOUND AT POSITION mid.
    else if(arr[mid] > x) // ELEMENT AT MID > X. SEARCH IN FIRST HALF
        return binarySearch(arr, l, mid-1, x);
    else                 // ELEMENT AT MID < X. SEARCH IN SECOND HALF
        return binarySearch(arr, mid+1, h, x);
}

int partition (int *arr, int low, int high)
{
    int pivot = arr[high];
    int i = (low - 1);   // INDEX OF LAST ELEMENT < pivot
    
    for(int j = low; j <= high- 1; j++)
    {
        // IF CURRENT ELEMENT <= pivot, PLACE IT AT (i+1) POSITION.
        if (arr[j] <= pivot)
        {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i+1], &arr[high]);
    return (i+1);
}

void quickSort(int *arr, int l, int h)
{
    if(l < h)
    {
        int m = partition(arr, l, h);
        quickSort(arr, l, m-1);
        quickSort(arr, m+1, h);
    }
}
// HELPER FUNCTIONS - END

int pairExist(int* arr, int n, int x)
{
    quickSort(arr, 0, n-1);

    for(int i=0; i<n-1; i++)
    {
        // SEARCH FOR arr[i] IN REST OF THE ARRAY.
        if(binarySearch(arr, i+1, n-1, x-arr[i]))
            return 1;
    }
    return 0;
}

int main()
{
    int arr[] = {3, 2, 7, 1, 9, 5, 8, 4};
    int x = 3;
    
    
    if(pairExist(arr, 8, x))
        printf("PAIR WITH SUM %d EXISTS IN THE ARRAY.", x);
    else
        printf("PAIR WITH SUM %d DOES NOT EXIST.", x);

    return 0;
}
