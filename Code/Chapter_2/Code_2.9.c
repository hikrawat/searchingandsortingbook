//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 2.9

#include <stdio.h>

#define RANGE 20
void checkPairs(int *arr, int n, int x)
{
    int hash[RANGE] = {0};
    
    // POPULATE HASH-MAP
    for(int i=0; i<n; i++)
        hash[arr[i]]++;
    
    for(int i=0; i<n; i++)
        if( x-arr[i]>=0 && x-arr[i]<RANGE-1 && hash[x-arr[i]]>=1)
        {
            printf("Pair Exist.");
            return;
        }
    printf("Pair does not Exist.");
} 

int main()
{
    int arr[] = {3, 2, 7, 1, 9, 5, 8, 4};
    int x = 13;
    
    checkPairs(arr, 8, x);
    
    return 0;
}