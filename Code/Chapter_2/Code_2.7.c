//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 2.7

#include <stdio.h>

int firstOcc(int *arr, int l, int h, int x)
{
    if(h >= l)
    {
        int mid = (l + h)/2;
        if( ( mid == 0 || x > arr[mid-1]) && arr[mid] == x)
            return mid;
        else if(x > arr[mid])
            return firstOcc(arr, (mid + 1), h, x);
        else
            return firstOcc(arr, l, (mid -1), x);
    }
    return -1;
}

int lastOcc(int *arr, int n, int l, int h, int x)
{
    if(h >= l)
    {
        int mid = (l + h)/2;
        if( ( mid == n-1 || x < arr[mid+1]) && arr[mid] == x )
            return mid;
        else if(x < arr[mid])
            return lastOcc(arr, n, l, (mid -1), x);
        else
            return lastOcc(arr, n, (mid + 1), h, x);
    }
    return -1;
}

int occurrenceCnt(int *arr, int x, int n)
{
    int idxFirst = firstOcc(arr, 0, n-1, x);
    
    // NUMBER DOES NOT EXIST
    if(idxFirst == -1){ return -1; }
    int idxLast = lastOcc(arr, n, idxFirst, n-1, x);
    return idxLast - idxFirst + 1;
}

int main()
{
    int arr[] = {1, 3, 3, 4, 4 ,5, 5, 5, 7, 8, 8};	int x = 5;
    
    printf("%d APPEARS %d TIMES", x, occurrenceCnt(arr, x, 11));

    return 0;
}
