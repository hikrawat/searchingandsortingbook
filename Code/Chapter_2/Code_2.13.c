//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 2.13

#include <stdio.h>

int ternarySearch(int arr[], int l, int h, int x)
{
    if(h<l)
        return -1;     // TERMINATING CONDITION
    
    // EACH PART HAS (h-l)/3 ELEMENTS
    int mid1 = l + (h - l)/3;
    int mid2 = mid1 + (h - l)/3;

    // x FOUND AT EITHER MID
    if(x == arr[mid1]) { return mid1; }
    if(x == arr[mid2]) { return mid2; }

    // x IN PART-1
    if(x < arr[mid1]){ return ternarySearch(arr, l, mid1-1, x); }

    // x IN PART-2
    if(x < arr[mid2]){ return ternarySearch(arr, mid1+1, mid2-1, x); }

    // x IN PART-3
    return ternarySearch(arr, mid2+1, h, x);
}

int main()
{
    int arr[] = {1, 3, 4, 5, 8, 10, 12, 14, 19, 25};
    
    int d;
    printf("Enter value to search :"); scanf("%d",&d);
    
    int pos = ternarySearch(arr, 0, 10, d);
    
    if(pos == -1)
        printf("%c NOT FOUND IN ARRAY.", d);
    else
        printf("%c FOUND AT POS %d.", d, pos);
}
