//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 2.12

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int data;
    struct node* left;
    struct node* right;
} Node;

int searchBST(Node *r, int x)
{
    if(r== NULL)
        return 0;          // ELEMENT NOT FOUND IN THE TREE.
    
    if(r->data == x)
        return 1;          // ELEMENT FOUND. SUCCESS.
    else if (r->data > x)
        return searchBST(r->left, x);
    else
        return searchBST(r->right, x);
}

Node* createNode(int v)
{
    Node* temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->left = temp->right = NULL;
    return temp;
}

void inOrder(Node * root)
{
    if(root == NULL) { return; } // TERMINATING CONDITION
    
    inOrder(root->left);
    printf("%d ", root->data);
    inOrder(root->right);
}

int main()
{
    Node* root = createNode(10);
    
    root->left = createNode(5);
    root->right = createNode(15);
    
    root->left->left = createNode(2);
    root->left->right = createNode(8);
    
    root->right->right = createNode(19);
    
    root->left->right->left = createNode(6);
    
    printf("IN-ORDER TRAVERSAL OF TREE :");
    inOrder(root);
    
    int d;
    printf("\nEnter value to search :"); scanf("%d",&d);
    
    if(searchBST(root, d))
        printf("%c FOUND", d);
    else
        printf("%c NOT FOUND IN ARRAY", d);
    
    return 0;
}
