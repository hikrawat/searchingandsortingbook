//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 2.4

#include <stdio.h>

int occurrenceCnt(int *arr, int n, int x)
{
    int cnt = 0;
    for(int i = 0; i<n; i++)
        if(arr[i] == x)
            cnt++;
    return cnt;
}

int main()
{
    int arr[] = {1, 3, 3, 4, 4 ,5, 5, 5, 7, 8, 8};	int x = 8;
    
    printf("%d APPEARS %d TIMES", x, occurrenceCnt(arr, 11, x));

    return 0;
}