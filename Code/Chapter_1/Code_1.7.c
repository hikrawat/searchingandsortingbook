//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 1.7

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int data;
    struct node* next;
} Node;

int linearSearch(Node* h, int x)
{
    while(h != NULL)
    {
        if(h->data == x)
            return 1;       // SUCCESS. x FOUND
        h = h->next;
    }
    return 0;           // FAILURE. x NOT FOUND.
} 

Node* addNode(int v)
{
    Node * temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->next = NULL;
    return temp;
}

void printList(Node* h)
{
    printf("\nLIST IS :");
    while(h != NULL){
        printf("%d ", h->data);
        h = h->next;
    }
}

int main()
{
    Node *h = addNode(5);
    h->next = addNode(6);
    h->next->next = addNode(1);
    h->next->next->next = addNode(6);
    h->next->next->next->next = addNode(2);
    h->next->next->next->next->next = addNode(5);
    
    printList(h);
    
    int d;
    printf("\nEnter value to be searched :"); scanf("%d", &d);
    
    if(linearSearch(h, d))
        printf("%d FOUND IN THE ARRAY", d);
    else
        printf("%d NOT FOUND IN THE ARRAY", d);
}

