//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 1.6

#include <stdio.h>

#define n 4
#define m 4

int linearSearch(int arr[m][n], int x)
{
    for(int i=0; i<m; i++)
    {
        for(int j=0; j<n; j++)
        {
            if(arr[i][j] == x)
                return 1;    // SUCCESS. ELEMENT FOUND AT POS (i,j)
        }
    }
    return 0; // FAILURE. ELEMENT NOT FOUND.
}

int main()
{
    int arr[m][n] = {{5, 7, 10, 5},
                     {1, 8,  3, 5},
                     {7, 2,  6, 8},
                     {12, 14, 9, 6}};
    int d;
    printf("Enter value to be searched "); scanf("%d", &d);
    
    if(linearSearch(arr, d))
        printf("%d NOT FOUND. ", d);
    else
        printf("%d FOUND IN THE ARRAY", d);
}