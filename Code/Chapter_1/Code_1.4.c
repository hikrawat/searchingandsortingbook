//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 1.4

#include <stdio.h>

int linearSearch(int * arr, int n , int x)
{
    for(int i=n-1; i>=0; i--)
    {
        if(arr[i] == x)
            return i;
    }
    return -1;
}

int main()
{
    int arr[] = {5, 7, 10, 5, 1, 8, 3, 5, 7, 2};
    int d;
    printf("Enter value to be searched "); scanf("%d", &d);
    int res = linearSearch(arr, 10, d);
    if(res == -1)
        printf("%d NOT FOUND. ", d);
    else
        printf("%d FOUND AT POS %d. ", d, res);
}