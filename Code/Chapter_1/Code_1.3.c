//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 1.3

#include <stdio.h>

int linearSearch(int * arr, int n , int x)
{
    if(n==0)
        return 0;    // ELEMENT NOT FOUND
    if(arr[0] == x)
        return 1;    // ELEMENT FOUND
    return linearSearch(arr+1, n-1, x);
}

int main()
{
    int arr[] = {3, 8, 1, 7, 4, 6, 2, 5, 0, 9};
    int d;
    printf("Enter value to be searched "); scanf("%d", &d);
    if(linearSearch(arr, 10, d) == -1)
        printf("%d NOT FOUND. ", d);
    else
        printf("%d FOUND IN THE ARRAY. ", d);
}