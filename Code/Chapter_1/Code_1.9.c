//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 1.9

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    char data;
    struct node* left;
    struct node* right;
} Node;

int linearSearch(Node* r, char x)
{
    if(r == NULL)
        return 0; // NOT FOUND
    
    if(r->data == x)
        return 1; // SUCCESS
    
    return (linearSearch(r->left, x) || linearSearch(r->right, x) );
} 

void preOrder(Node * root)
{
    if(root == NULL) { return; } // TERMINATING CONDITION
    
    printf("%c ", root->data);
    preOrder(root->left);
    preOrder(root->right);
}

Node* createNode(char v)
{
    Node* temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->left = temp->right = NULL;
    return temp;
}

int main()
{
    Node* root = createNode('A');
    
    root->left = createNode('B');
    root->right = createNode('C');
    
    root->left->left = createNode('D');
    root->left->right = createNode('E');
    
    root->right->left = createNode('F');
    root->right->right = createNode('G');
    
    printf("PRE-ORDER TRAVERSAL OF TREE :");
    preOrder(root);
    
    char d;
    printf("\nEnter value to search :");
    fflush(stdin);
    scanf("%c",&d);
    
    if(linearSearch(root, d))
        printf("%c FOUND", d);
    else
        printf("%c NOT FOUND IN ARRAY", d);
    
    return 0;
}