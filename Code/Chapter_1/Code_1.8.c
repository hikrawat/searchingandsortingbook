//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 1.8

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int data;
    struct node* next;
} Node;

int linearSearch(Node* h, int x)
{
    if(h == NULL)
        return -1;    // NOT FOUND
    
    if(h->data == x)
        return 0;     // SUCCESS
    
    int pos = linearSearch(h->next, x); // SEARCH IN REST OF THE LIST.
    
    if(pos == -1)
        return -1;    // ELEMENT NOT FOUND IN REST OF LIST
    else
        return pos+1; // ADDING 1 FOR CURRENT NODE. SUCCESS.
} 

Node* addNode(int v)
{
    Node * temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->next = NULL;
    return temp;
}

void printList(Node* h)
{
    printf("\nLIST IS :");
    while(h != NULL){
        printf("%d ", h->data);
        h = h->next;
    }
}

int main()
{
    Node *h = addNode(5);
    h->next = addNode(6);
    h->next->next = addNode(1);
    h->next->next->next = addNode(6);
    h->next->next->next->next = addNode(2);
    h->next->next->next->next->next = addNode(5);
    
    printList(h);
    
    int d;
    printf("\nEnter value to be searched :"); scanf("%d", &d);
    int pos = linearSearch(h, d);
    if(pos == -1)
        printf("%d FOUND IN THE ARRAY", d);
    else
        printf("%d NOT FOUND IN ARRAY AT POS %d", d, pos);
}
