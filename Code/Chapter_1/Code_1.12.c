//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 1.12

#include <stdio.h>
#include <stdlib.h>

int NUM = 35;

// CHECK IF n CONTAIN DIGITS OF NUM
int containDigits(int n)
{
    // CHECK IF TWO CONSECUTIVE DIGITS = 35
    while(n != 0)
    {
        if(n%100 == NUM)
            return 1;
        n = n/10;
    }
    return 0;
}

int isDivisibleBy(int n)
{
    return (n%NUM == 0);
}

void printLikeDislike(int *a, int n)
{
    for(int i=0;i<n;i++)
    {
        if(containDigits(a[i]) || isDivisibleBy(a[i]))
            printf("LIKE ");
        else
            printf("DISLIKE ");
    }
}

int main()
{
    int arr[] = {210,2351,120,245,3456,357};
    printLikeDislike(arr, 6);
    return 0;
}