//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 1.5

#include <stdio.h>

void linearSearch(int * arr, int n , int x)
{
    for(int i=0; i<n; i++)
    {
        if(arr[i] == x)
            printf("%d ", i);
    }
}

int main()
{
    int arr[] = {5, 7, 10, 5, 1, 8, 3, 5, 7, 2};
    int d;
    printf("Enter value to be searched "); scanf("%d", &d);
    linearSearch(arr, 10, d);
}