//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 1.11

#include <stdio.h>
#include <stdlib.h>

// LINEAR SEARCH IN LINKED LIST
typedef struct node
{
    int data;
    struct node* next;
} Node;

Node* addNode(int v)
{
    Node * temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->next = NULL;
    return temp;
}

int moveFirstSearch(Node **hp, int x)
{
    if(hp == NULL || *hp == NULL)
        return 0; // LIST EMPTY
    
    Node* h = *hp;
    
    if(h->data == x)
        return 1; // ELEMENT FOUND AT HEAD
    
    // MOVING AHEAD TILL ELEMENT AT NEXT NODE
    while(h->next != NULL && h->next->data!= x)
        h = h->next;
    
    if(h->next == NULL)
        return 0; // ELEMENT NOT FOUND IN LIST
    
    Node* temp = h->next;
    h->next = temp->next;
    
    temp->next = *hp;
    *hp = temp;
    return 1;
}

void printList(Node* h)
{
    printf("\nLIST IS :");
    while(h != NULL){
        printf("%d ", h->data);
        h = h->next;
    }
}

int main()
{
    Node *h = addNode(5);
    h->next = addNode(6);
    h->next->next = addNode(1);
    h->next->next->next = addNode(6);
    h->next->next->next->next = addNode(2);
    h->next->next->next->next->next = addNode(5);
    
    printList(h);
    int d;
    printf("\nEnter value to search in list :");scanf("%d", &d);
    
    if(moveFirstSearch(&h, d))
        printf("%d FOUND AND MOVED TO HEAD OF LIST.", d);
    else
        printf("%d NOT FOUND IN LIST.", d);
    
    printList(h);
}
