//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 1.13

#include <stdio.h>

int getMax(int *arr, int n)
{
    int max = arr[0];
    for(int i=0; i<n; i++)
        if(arr[i] > max)
            max = arr[i];
    return max;
}

int main()
{
    int arr[] = {210,2351,120,245,3456,357};
    printf("Max element in the given array is :%d", getMax(arr, 6));
    return 0;
}