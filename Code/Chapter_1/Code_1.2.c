//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 1.2

#include <stdio.h>

int linearSearch(int * arr, int n , int x)
{
    for(int i=0; i<n; i++)
    {
        if(arr[i] == x)
            return i;
    }
    return -1;
}

int main()
{
    int arr[] = {3, 8, 1, 7, 4, 6, 2, 5, 0, 9};
    int d;
    printf("Enter value to be searched "); scanf("%d", &d);
    int res = linearSearch(arr, 10, d);
    if(res == -1)
        printf("%d NOT FOUND. ", d);
    else
        printf("%d FOUND AT POS %d. ", d, res);
}