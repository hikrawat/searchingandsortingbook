//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 1.10

#include <stdio.h>
#include <string.h>

int naiveSearch(char* str, char* pattern)
{
    // NUMBER OF CHAR IN STRING AND PATTERN
    size_t n = strlen(str); size_t m = strlen(pattern);
    
    for(int i=0; i <= n-m; i++)
    {
          int j;

        // CHECK IF PATTERN START FROM INDEX i
        for(j=0; j<m; j++)
            if (str[i+j] != pattern[j])
                break;

        if(j == m)
            return 1;   // PATTERN FOUND
    }
    return 0; // PATTERN NOT FOUND
}

int main()
{
    char str[] = "Ritambhara Technologies for Coding Interviews";
    char pat[] = "Tech";
    if(naiveSearch(str, pat))
        printf("PATTERN FOUND. ");
    else
        printf("PATTERN NOT FOUND IN STRING. ");
}