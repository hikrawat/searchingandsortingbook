//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 1.1

#include <stdio.h>

int linearSearch(int * arr, int n , int x)
{
    for(int i=0; i<n; i++)
    {
        if(arr[i] == x)
            return 1;
    }
    return 0;
}

int main()
{
    int arr[] = {3, 8, 1, 7, 4, 6, 2, 5, 0, 9};
    int d;
    printf("Enter value to be searched "); scanf("%d", &d);
    if(linearSearch(arr, 10, d) == -1)
        printf("%d NOT FOUND. ", d);
    else
        printf("%d FOUND IN THE ARRAY. ", d);
}