//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 3.8

#include <stdio.h>

int searchMissing(int* arr, int n)
{
    for(int x=1; x<=n+1; x++)
    {
        int i=0;
        for(; i<n; i++)
        {
            if(arr[i] == x)
                break;
        }
        if(i==n)
            return x;
    }
    return -1; // CONTROL NEVER REACH HERE
}

int main()
{
    int arr[] = {8, 3, 5, 7, 4, 6, 1};

    printf("MISSING ELEMENT : %d", searchMissing(arr, 7));

    return 0;
}