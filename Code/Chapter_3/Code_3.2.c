//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 3.2

#include <stdio.h>

// HELPER FUNCTIONS - START
void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

int partition (int *arr, int low, int high)
{
    int pivot = arr[high];
    int i = (low - 1);   // INDEX OF LAST ELEMENT < pivot
    
    for(int j = low; j <= high- 1; j++)
    {
        // IF CURRENT ELEMENT <= pivot, PLACE IT AT (i+1) POSITION.
        if (arr[j] <= pivot)
        {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i+1], &arr[high]);
    return (i+1);
}

void quickSort(int *arr, int l, int h)
{
    if(l < h)
    {
        int m = partition(arr, l, h);
        quickSort(arr, l, m-1);
        quickSort(arr, m+1, h);
    }
}

int binarySearch(int *arr, int l, int h, int x)
{
    if(l>h)
        return 0;           // ELEMENT NOT FOUND.
    
    int mid = (l+h)/2;
    if(arr[mid] == x)
        return 1;           // ELEMENT FOUND AT POSITION mid.
    else if(arr[mid] > x) // ELEMENT AT MID > X. SEARCH IN FIRST HALF
        return binarySearch(arr, l, mid-1, x);
    else                 // ELEMENT AT MID < X. SEARCH IN SECOND HALF
        return binarySearch(arr, mid+1, h, x);
}

void printArray(int *arr, int n)
{
    printf("\nArray is :");
    for(int i=0; i<n; i++)
        printf("%d ", arr[i]);
}
// HELPER FUNCTIONS - END

void printMissing(int *arr, int n, int low, int high)
{
    quickSort(arr, 0, n-1); // SORT THE ARRAY
    int i = binarySearch(arr, 0, n-1, low);
    int x = low;

    while (i < n && x<=high)
    {
        if(arr[i] != x) // x DOES NOT PRESENT
            printf("%d ", x);
        else// x IS PRESENT
            i++;
            
        // MOVE TO NEXT ELEMENT IN RANGE [low..high]
        x++;
    }
    
    // PRINT LEFT OVER ELEMENTS
    while(x <= high)
    {
        printf("%d ", x);
        x++;
    }
}

int main()
{
    int arr[] = {4, 1, 6, 12, 57, 7, 10};
    int low=4, high=12;
    
    printMissing(arr, 7, low, high);
}