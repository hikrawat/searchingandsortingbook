//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 3.10

#include <stdio.h>

void getMissingAndRepeating(int *arr, int n)
{
    int xors = 0;
    int i;
    int x = 0;
    int y = 0;
    
    // XOR OF ALL ELEMENTS IN ARRAY
    for(i=0; i<n; i++)
        xors = xors ^ arr[i];
    
    // XOR OF NUMBERS FROM 1 TO n
    for(i=1; i<=n; i++)
        xors = xors ^ i;
    
    int setBitNum = xors & ~ (xors-1);
    
    // DIVIDING NUMBERS IN TWO SETS AND GETTING THE XORs
    for(i = 0; i < n; i++)
    {
        if(arr[i] & setBitNum)
            x = x ^ arr[i];      // arr[i] BELONGS TO SET A
        else
            y = y ^ arr[i];      // arr[i] BELONGS TO SET B
    }
    
    for(i = 1; i <= n; i++)
    {
        if(i & setBitNum)
            x = x ^ i;           // arr[i] BELONGS TO SET A
        else
            y = y ^ i;           // arr[i] BELONGS TO SET B
    }
    
    printf("Repeating : %d \n Missing Number : %d", x, y);
}

int main()
{
    int arr[] = {1, 5, 3, 4, 1, 2};

    getMissingAndRepeating(arr, 6);

    return 0;
}