//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 3.7

#include <stdio.h>
#include <string.h>

void fillLPSArray(char* str, int* lps)
{
    int i = 1, j = 0;
    size_t n = strlen(str);
    lps[0] = 0;
    while(i<n)
    {
        if(str[i] == str[j])
        {
            lps[i] = j+1;
            i++; j++;
        }
        else
        {
            if(j==0)
            {
                lps[i] = 0; i++;
            }
            else
            { 
                j = lps[j-1]; 
            }
        }
    }
}

void KMPSearch(char *str, char *pattern)
{
    int N = strlen(str);
    int M = strlen(pattern);
    
    // LPS ARRAY
    int lps[M];
    fillLPSArray(pattern, lps);
    
    for(int i=0, j=0; i<N; )
    {
        if(str[i] == pattern[j])
        {
            j++; i++;
        }
        
        if(j == M)
        {
            printf("FOUND PATTERN AT POSITION: %d\n", i-j);
            return;
        }
        else if (i < N && pattern[j] != str[i])
        {
            if(j != 0)
                j = lps[j-1];
            else
                i = i+1;
        }
    }
    printf("PATTERN NOT FOUND");
}

void printArray(int *arr, int n)
{
    for(int i=0; i<n; i++)
        printf("%d ", arr[i]);
}

int main()
{
    char str[] = "ADBADCCADBADX";
    char pat[] = "ADBADX";

    KMPSearch(str, pat);

    return 0;
}