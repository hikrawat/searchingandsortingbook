//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 3.5

#include <stdio.h>
#include <string.h>

#define NUM 13
#define true 1
#define false 0

int computeHash(char* str, unsigned long n)
{
    int sum = 0;
    for(int i=0; i<n; i++)
        sum += str[i]-'a'+1;
    
    return sum%NUM;
}

int isEqual(char *a, char*b, unsigned int n)
{
    for(int i=0; i<n; i++, a++, b++)
        if(*a != *b)
            return false;
    
    return true;
}

int rabinKarp(char* str, char* pattern)
{
    size_t n = strlen(str);
    size_t m = strlen(pattern);

    int hashArr[n-m+1];
    hashArr[0] = computeHash(str, m);

    for(int i=1; i<n-m+1; i++)
        hashArr[i] = (hashArr[i-1]-str[i-1]+str[i+m-1])%13;

    int patternHash = computeHash(pattern, m);

    for(int i=0; i<n-m+1; i++)
        if(hashArr[i] == patternHash && isEqual(pattern, str+i, m))
            return i;
    return -1;
}

int main()
{
    char str[] = "Ritambhara Technologies for coding interview";
    char pat[] = "Tech";
    int pos = rabinKarp(str, pat);
    
    if(pos == -1)
        printf("PATTERN NOT PRESENT IN THE STRING");
    else
        printf("PATTERN PRESENT AT POS: %d", pos);
    
    return 0;
}