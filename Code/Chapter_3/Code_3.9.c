//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 3.9

#include <stdio.h>

int searchMissingHash(int * arr, int n)
{
    int hash[n];	// HASH TO STORE COUNT
    
    // INITIALIZE WITH ZERO
    for(int i=0; i<n; i++)
        hash[i] = 0;
    
    // POPULATE HASH
    for(int i=0; i<n; i++)
        hash[arr[i]-1]++;
    
    // CHECK HASH
    for(int i=0; i<n; i++)
        if(hash[i] == 0)
            return i+1;
    
    return -1;  // UNREACHABLE CODE
}

int main()
{
    int arr[] = {8, 3, 5, 7, 4, 6, 1};

    printf("MISSING ELEMENT : %d", searchMissingHash(arr, 7));

    return 0;
}
