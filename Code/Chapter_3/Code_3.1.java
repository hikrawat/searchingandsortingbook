//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.
// Rename this file as RTDemo.java
//  Code 3.1. 

import java.util.*; 
public class RTDemo
{
	public static void main(String args[])
	{ 
		HashMap<Integer,String> hash = new HashMap<Integer,String>();
			
		hash.put(1000, "Ritambhara");
		hash.put(2000, "Moksha"); 
		hash.put(3000, "Radha"); 
		
		System.out.println("Value for 1000 is: " + hash.get(1000));
		System.out.println("Value for 2000 is: " + hash.get(2000));
		System.out.println("Value for 3000 is: " + hash.get(3000));
	}
}