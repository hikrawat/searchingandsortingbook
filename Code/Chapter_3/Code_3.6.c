//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 3.6

#include <stdio.h>
#include <string.h>

void fillLPSArray(char* str, int* lps)
{
    int i = 1, j = 0;
    size_t n = strlen(str);
    lps[0] = 0;
    while(i<n)
    {
        if(str[i] == str[j])
        {
            lps[i] = j+1;
            i++; j++;
        }
        else
        {
            if(j==0)
            {
                lps[i] = 0; i++;
            }
            else
            { 
                j = lps[j-1]; 
            }
        }
    }
}

void printArray(int *arr, int n)
{
    for(int i=0; i<n; i++)
        printf("%d ", arr[i]);
}

int main()
{
    char pat[] = "ADBADX";
    int n = strlen(pat);
    int lps[n];
    fillLPSArray(pat, lps);
    
    printArray(lps, n);
    
    return 0;
}