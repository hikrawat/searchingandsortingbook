//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 3.11

#include <stdio.h>

int getLargest(int *arr, int n)
{
    if(n <= 0) { return -1; } // INVALID ARRAY.
    
    int max = arr[0];
    for(int i=1; i<n; i++)
        if(arr[i]>max)
            max = arr[i];
    return max;
}

int main()
{
    int arr[] = {1, 5, 3, 4, 1, 2};

    printf("LARGEST : %d",getLargest(arr, 6));

    return 0;
}