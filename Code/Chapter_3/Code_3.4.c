//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 3.4

#include <stdio.h>

#define NUM 13

int computeHash(char* str, unsigned int n)
{
    int sum = 0;
    for(int i=0; i<n; i++)
        sum += str[i]-'a'+1;
    
    return sum%NUM;
}

int main()
{
    return 0;
}