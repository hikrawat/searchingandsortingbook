//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 3.3

#include <stdio.h>

void printMissing(int *arr, int n, int low, int high)
{
    int HASH_SIZE = high-low+1;
    int hash[HASH_SIZE];

    // INITIALIZING HASH
    for(int i=0; i<HASH_SIZE; i++)
        hash[i] = 0;

    // POPULATING HASH
    for(int i=0; i<n; i++)
        if(arr[i]>=low && arr[i]<=high)
            hash[arr[i]-low]++;
    
    // TRAVERSING HASH TO SEE MISSING ELEMENT
    for(int i=0; i<HASH_SIZE; i++)
        if(hash[i] == 0)
            printf("%d ", i+low);
}

int main()
{
    int arr[] = {4, 1, 6, 12, 57, 7, 10};
    int low=4, high=12;
    
    printMissing(arr, 7, low, high);
}