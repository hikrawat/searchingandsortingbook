//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 6.1

#include <stdio.h>

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

void insertionSort(int *a, int n)
{
    for(int i=1; i<n; i++)
    {
        int j, temp = a[i];
        for(j=i-1; j>=0 && a[j] > temp; j--)
            a[j+1] = a[j];
        a[j+1] = temp;
    }
}

int main()
{
    int arr[] = {9, 0, 1, 6, 3, 2, 4, 7, 5, 8};
    
    printf("\nstudent Knowedge :"); printArray(arr, 10);
    insertionSort(arr, 10);
    printf("Open Positions   :"); printArray(arr, 10);
}