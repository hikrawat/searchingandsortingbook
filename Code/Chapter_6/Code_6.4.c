//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 6.4

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int data;
    struct node* next;
} Node;

void insertSorted(Node** headPtr, Node* newNode)
{
    // PARANOID CHECK. NOTHING TO INSERT
    if(newNode == NULL){ return; }
    
    // INSERTING NODE AT HEAD.
    if(*headPtr == NULL || (*headPtr)->data > newNode->data)
    {
        newNode->next = *headPtr;
        *headPtr = newNode;
        return;
    }
    
    Node* head = *headPtr;     // POINTER TO FIRST NODE OF LIST
    
    // MOVE TO POINT OF INSERTION
    while( head->next != NULL && head->next->data < newNode->data)
        head = head->next;
    
    newNode->next =  head->next;
    head->next = newNode;
}

void sortList(Node** headPtr)
{
    if(headPtr == NULL || (*headPtr) == NULL)
        return;
    
    Node* head = *headPtr;  // HEAD OF LIST
    Node* headSorted = NULL;    // HEAD OF SORTED LIST
    
    // FOR EACH NODE IN THE LIST.
    while(head != NULL)
    {
        Node * temp = head->next;
        insertSorted(&headSorted, head);
        head = temp;
    }
    *headPtr = headSorted;
}

Node* addNode(int v)
{
    Node * temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->next = NULL;
    return temp;
}

void printfList(Node* head)
{
    while(head != NULL)
    {
        printf("%d ", head->data);
        head = head->next;
    }
}

int main()
{
    Node *h = addNode(9);
    h->next = addNode(6);
    h->next->next = addNode(1);
    h->next->next->next = addNode(4);
    h->next->next->next->next = addNode(2);
    h->next->next->next->next->next = addNode(5);
    
    printf("ORIGINAL LIST :");printfList(h);
    sortList(&h);
    printf("\nSORTED LIST   :");printfList(h);
    
    return 0;
}
