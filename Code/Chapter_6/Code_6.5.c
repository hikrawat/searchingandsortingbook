//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 6.5

#include <stdio.h>

void shellSort(int *a, int n)
{
    unsigned long gaps[] = {861, 336, 112, 48, 21, 7, 3, 1}; // GAP SEQUENCE
    for(unsigned long k=0; k<16; k++)
    {
        unsigned long h = gaps[k];
        for (unsigned long i=h; i<n; i++)
        {
            int v=a[i];
            unsigned long j;
            for(j=i; j>=h && a[j-h]>v; j=j-h)
                a[j]=a[j-h];
            a[j]=v;
        }
    }
}

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

int main()
{
    int arr[] = {9, 0, 1, 6, 3, 2, 4, 7, 5, 8};
    
    printf("\nUNSORTED ARRAY :"); printArray(arr, 10);
    shellSort(arr, 10);
    printf(“SORTED ARRAY   :"); printArray(arr, 10);
    
    return 0;
}
