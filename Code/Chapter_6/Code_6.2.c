//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 6.2

#include <stdio.h>

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

void insertionSortRec(int *arr, int n)
{
    // TERMINATING CONDITION
    if(n <= 1){ return; }
    
    // SORT FIRST n-1 ELEMENTS RECURSIVELY
    insertionSortRec( arr, n-1 );
    
    // MOVING LAST ELEMENT TO IT'S RIGHT POSITION
    int j, temp = arr[n-1];
    for(j=n-2; j>=0 && arr[j] > temp; j--)
        arr[j+1] = arr[j];
    arr[j+1] = temp;
}

int main()
{
    int arr[] = {9, 0, 1, 6, 3, 2, 4, 7, 5, 8};
    
    printf("\nstudent Knowedge :"); printArray(arr, 10);
    insertionSortRec(arr, 10);
    printf("Open Positions   :"); printArray(arr, 10);
}
