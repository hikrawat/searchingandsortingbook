//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 6.3

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int data;
    struct node* next;
} Node;

void insertSorted(Node** headPtr, Node* newNode)
{
    // PARANOID CHECK. NOTHING TO INSERT
    if(newNode == NULL){ return; }
    
    // INSERTING NODE AT HEAD.
    if(*headPtr == NULL || (*headPtr)->data > newNode->data)
    {
        newNode->next = *headPtr;
        *headPtr = newNode;
        return;
    }
    
    Node* head = *headPtr;     // POINTER TO FIRST NODE OF LIST
    
    // MOVE TO POINT OF INSERTION
    while( head->next != NULL && head->next->data < newNode->data)
        head = head->next;
    
    newNode->next =  head->next;
    head->next = newNode;
}  

Node* addNode(int v)
{
    Node * temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->next = NULL;
    return temp;
}

void printfList(Node* head)
{
    while(head != NULL)
    {
        printf("%d ", head->data);
        head = head->next;
    }
}

int main()
{
    Node *h = addNode(9);
    
    Node *temp = addNode(6);
    insertSorted(&h, temp);

    temp = addNode(1);
    insertSorted(&h, temp);

    temp = addNode(4);
    insertSorted(&h, temp);

    temp = addNode(2);
    insertSorted(&h, temp);

    temp = addNode(5);
    insertSorted(&h, temp);

    printfList(h);
}
