//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 0.18

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int data;
    struct node* left;
    struct node* right;
} Node;

void inOrder(Node *root)
{
    if(root == NULL) { return; } // TERMINATING CONDITION
    inOrder(root->left);
    printf("%d ", root->data);
    inOrder(root->right);
}

Node* createNode(int v)
{
    Node* temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->left = temp->right = NULL;
    return temp;
}

int main()
{
    Node* root = createNode(5);

    root->left = createNode(3);
    root->right = createNode(20);

    root->left->left = createNode(1);
    root->left->right = createNode(4);

    root->right->left = createNode(15);
    root->right->right = createNode(25);
    inOrder(root);
    return 0;
}