//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 0.1

#include <stdio.h>

int sum(unsigned int n)
{
    // FIRST TERMINATING CONDITION
    if(n == 0)
        return 0;
    
    // SECOND TERMINATING CONDITION
    if(n == 1)
        return 1;
    
    return n + sum(n-1);
}

int main()
{
    int n;
    scanf("%d", &n);
    printf("Sum of First %d natural numbers = %d", n, sum(n));
    return 0;
}