//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 0.13

#include <stdio.h>

size_t strlen(const char *s)
{
    int cnt = 0;
    while(s != NULL && *s != '\0')
    {
        cnt++; s++;
    }
    return cnt;
}

int main()
{
    char str[] = "Ritambhara Technologies";
    printf("Length of '%s' : %ld ", str, strlen(str));
    return 0;
}