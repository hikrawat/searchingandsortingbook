//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 0.5

#include <stdio.h>

long power(int a, unsigned int n)
{
    long prod = 1, i=0;
    for(; i<n; i++)
        prod = prod * a;
    
    return prod;
}
int main()
{
    int a;
    unsigned int n;
    printf("Enter a "); scanf("%d", &a);
    printf("\nEnter n "); scanf("%d", &n);
     
    printf("\nPower of %d^%d = %ld", a, n, power(a, n));
     
    return 0;
}