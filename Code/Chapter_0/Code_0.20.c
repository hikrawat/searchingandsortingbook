//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 0.20

#include <stdio.h>

#define N 5

char vertex[N] = {'A', 'B', 'C', 'D', 'E'};
int edges[N][N] = { {0, 1, 0, 0, 1},
                    {1, 0, 0, 1, 0},
                    {0, 0, 0, 1, 1},
                    {0, 1, 1, 0, 1},
                    {1, 0, 1, 1, 0} };