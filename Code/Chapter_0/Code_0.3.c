//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 0.3

#include <stdio.h>

int power(int x, int n)
{
    if(0 == n || 1 == x){ return 1; }
    return x * power(x, n-1);
}


int main()
{
    int n, x;
    printf("Enter x "); scanf("%d", &x);
    printf("\nEnter n "); scanf("%d", &n);
    
    printf("\nPower of %d^%d = %d", x, n, power(x, n));
    
    return 0;
}