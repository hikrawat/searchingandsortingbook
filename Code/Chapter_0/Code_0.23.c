//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 0.23

#include <stdio.h>
#include <stdlib.h>

#define bool int
#define false 0
#define true 1

#define N 5

typedef struct linkNode
{
    int data; // INDEX OF ADJACENT VERTEX
    struct  linkNode* next;
}LinkNode;

typedef struct vertextInfo
{
    char data;
    LinkNode *head;
}VertextInfo;

VertextInfo vertex[N];

bool isAdjascent(char v1, char v2)
{
    int i = getVertexId(v1);
    int j = getVertexId(v2);
    
    // INVALID VERTEX
    if(i == -1 || j == -1) { return false; }

    // TRAVERSE ADJ. LIST OF Vertex-i
    LinkNode* head = vertex[i].head;
    while(head != NULL)
    {
        if(head->data == j)
            return true;
        head = head->next;
    }
    return false;
}

void printAllAdjescent(char v1)
{
    int i = getVertexId(v1);
    if(i == -1){ return; } // INVALID VERTEX
    
    // TRAVERSE ADJ. LIST OF Vertex-i
    LinkNode* head = vertex[i].head;
    while(head != NULL)
    {
        printf(" %c", vertex[head->data].data);
        head = head->next;
    }
}