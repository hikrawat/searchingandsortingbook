//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 0.22

#include <stdio.h>
#include <stdlib.h>

#define N 5

char vertex[N] = {'A', 'B', 'C', 'D', 'E'};
int edges[N][N] = { {0, 1, 0, 0, 1},
                    {1, 0, 0, 1, 0},
                    {0, 0, 0, 1, 1},
                    {0, 1, 1, 0, 1},
                    {1, 0, 1, 1, 0} };

int getVertexId(char v)
{
    for(int i=0; i<N; i++)
        if(vertex[i] == v)
            return i;
    return -1;
}

void printAllAdjescent(char v1)
{
    int i = getVertexId(v1);
    if(i == -1){ return; } // INVALID VERTEX
    
    for(int j=0; j<N; j++)
        if(edges[i][j] != 0)
            printf(" %c", vertex[j]);
}


int isAdjascent(char v1, char v2)
{
    int i = getVertexId(v1), j = getVertexId(v2);

    // INVALID VERTEX
    if(i == -1 || j == -1) { return 0; }

    return edges[i][j];
}

int main()
{
    printAllAdjescent('E');
    return 0;
}