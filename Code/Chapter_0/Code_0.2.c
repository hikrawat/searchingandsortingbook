//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 0.2

#include <stdio.h>

int sum(int n)
{
    int sum = 0;
    for(int i=1; i<=n; i++)
        sum += i;
    return sum;
}

int main()
{
    int n;
    scanf("%d", &n);
    printf("Sum of First %d natural numbers = %d", n, sum(n));
    return 0;
}