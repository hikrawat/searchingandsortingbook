//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 0.16

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int data;
    struct node *next;
} Node;

Node* insertAtHead(Node *head, int x)
{
    Node *temp = (Node*) malloc(sizeof(Node));
    temp->data = x;
    temp->next = head;
    return temp;
}

void printList(Node *head)
{
    while(head != NULL)
    {
        printf("%d ", head->data);
        head = head->next;
    }
}

int main()
{
    Node* head = NULL;
    head = insertAtHead(head, 5);
    head = insertAtHead(head, 10);
    head = insertAtHead(head, 15);
    printList(head);
    return 0;
}