//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 0.10

#include <stdio.h>

int main()
{
    unsigned int X, Y;
    printf("Enter First Number (X):"); scanf("%d", &X);
    printf("Enter Second Number (Y): "); scanf("%d", &Y);
     
    X = X + Y;
    Y = X - Y;
    X = X - Y;

    printf("First Number(X) : %d\nSecond Number(Y): %d", X, Y);
     
    return 0;
}