//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 0.8

#include <iostream>
using namespace std;

void swap(int &a, int &b)
{
    int temp = a; a = b; b = temp;
}

int main()
{
    int a, b;
    
    cout<<"Enter First Number (a):"; cin>>a;
    cout<<"Enter Second Number (b): ";  cin>>b;
    
    swap(a, b);
    cout<<"First Number(a) : "<<a<<endl;
    cout<<"Second Number(b): "<<b<<endl;
    
    return 0;
}