//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 0.21

#include <stdio.h>

#define N 5

char vertex[N] = {'A', 'B', 'C', 'D', 'E'};
int edges[N][N] = { {0, 1, 0, 0, 1},
                    {1, 0, 0, 1, 0},
                    {0, 0, 0, 1, 1},
                    {0, 1, 1, 0, 1},
                    {1, 0, 1, 1, 0} };

int getVertexId(char v)
{
    for(int i=0; i<N; i++)
        if(vertex[i] == v)
            return i;
    return -1;
}

int isAdjascent(char v1, char v2)
{
    int i = getVertexId(v1), j = getVertexId(v2);

    // INVALID VERTEX
    if(i == -1 || j == -1) { return 0; }

    return edges[i][j];
}

int main()
{
    printf("isAdjascent('A', 'E'): %d", isAdjascent('A', 'E'));
    return 0;
}