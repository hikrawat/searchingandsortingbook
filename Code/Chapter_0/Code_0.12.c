//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 0.12

#include <stdio.h>

void printArray(int *arr, int n)
{
    for(int i=0; i<n; i++)
        printf("%d ", arr[i]);
}

int main()
{
    int arr[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    printArray(arr, 10);
    return 0;
}