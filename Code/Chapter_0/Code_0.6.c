//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 0.6

#include <stdio.h>

long power(int a, int n)
{
    // TERMINATING CONDITIONS
    if(a==0){ return 0; }
    if(n==0){ return 1; }
    
    if((n&1) == 0)
        return power(a*a, n/2);      // n IS EVEN
    else
        return a * power(a*a, n/2);  // n IS ODD
}

int main()
{
    int a;
    unsigned int n;
    printf("Enter a "); scanf("%d", &a);
    printf("\nEnter n "); scanf("%d", &n);
     
    printf("\nPower of %d^%d = %ld", a, n, power(a, n));
     
    return 0;
}