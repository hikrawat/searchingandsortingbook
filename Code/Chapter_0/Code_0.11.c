//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 0.11

#include <stdio.h>
#include <string.h>

// str1 AND str2 ARE OF EQUAL LENGTH
void swap(char* str1, char* str2)
{
    size_t len = strlen(str1);
    for(int i=0; i<len; i++)
    {
        char temp = str1[i];
        str1[i] = str2[i];
        str2[i] = temp;
    }
}

int main()
{
    char str1[] = "ABCD";
    char str2[] = "PQRS";
    swap(str1,str2);
    printf("str1 : %s\nstr2 : %s", str1, str2);
    return 0;
}