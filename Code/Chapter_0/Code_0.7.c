//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 0.7

#include <stdio.h>

void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

int main()
{
    int a, b;
    printf("Enter First Number (a):"); scanf("%d", &a);
    printf("Enter Second Number (b): "); scanf("%d", &b);
     
    swap(&a, &b);
    printf("First Number(a) : %d\nSecond Number(b): %d", a, b);
     
    return 0;
}