//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 11.5

#include <iostream>
using namespace std;

#define N 3
void sortRowColDiagonal(int arr[][N])
{
    // oneDimArray IS A POINTER TO 1-DIM ARRAY.
    int *oneDimArray = (int *)arr;
    
    // PASSING POINTER TO SATRT AND END OF ARRAY.
    std::sort(oneDimArray, oneDimArray + N*N);
}

void printMatrix(int arr[][N])
{
    for (int i=0; i<N; i++)
    {
        for (int j=0; j<N; j++)
            cout << arr[i][j] << " ";
        cout <<"\n";
    }
}

int main()
{
    int arr[N][N]={{9, 5, 8},
        {2, 4, 1},
        {3, 7, 6}};
    
    sortRowColDiagonal(arr);
    printMatrix(arr);
    return 0;
}