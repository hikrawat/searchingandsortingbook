//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 11.2

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int data;
    struct node* next;
} Node;

Node* addNode(int v)
{
    Node * temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->next = NULL;
    return temp;
}

void printfList(Node* head)
{
    while(head != NULL)
    {
        printf("%d ", head->data);
        head = head->next;
    }
}

Node* sortList(Node* head)
{
    // IF LIST HAS ZERO OR ONE NODE
    if(head == NULL || head->next == NULL)
        return NULL;
    
    // POINTER TO POINT LAST NODE OF LIST
    Node* last = head;
    
    int numOfNodes = 1;
    while(last->next != NULL)
    {
        last = last->next;
        numOfNodes++;
    }
    
    Node *tail = last;
    Node *ptr = head;
    Node *prev = head;
    for(int i=0; i<numOfNodes; i++)
    {
        Node* temp = ptr;
        ptr = ptr->next;
        if(temp->data == 0)
        {
            // INSERT AT HEAD
            if(prev != temp)
            {
                temp->next = head;
                head = temp;
                prev->next = ptr;
            }
        }
        else if(temp->data == 2)
        {
            // INSERT AT END.
            tail->next = temp;
            temp->next = NULL;
            tail = temp;
            
            // IF FIRST NODE.
            if(prev == temp)
                head = prev = ptr;
            else
                prev->next = ptr;
        }
        else
        {
            if(prev != temp)
                prev = prev->next;
        }
    }
    
    return head;
}

int main()
{
    Node *h = addNode(1);
    h->next = addNode(1);
    h->next->next = addNode(2);
    h->next->next->next = addNode(0);
    h->next->next->next->next = addNode(2);
    h->next->next->next->next->next = addNode(0);
    h->next->next->next->next->next->next = addNode(1);
    h->next->next->next->next->next->next->next = addNode(0);
    
    printf("ORIGINAL LIST : "); printfList(h);
    h = sortList(h);
    printf("\nSORTED LIST   : "); printfList(h);
    
    return 0;
}