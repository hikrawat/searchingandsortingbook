//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 11.4

#include <stdio.h>

void swap(int* a, int*b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}
void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

// FUNCTION TO FLIP ELEMENTS FROM INDEX 0 to i (both inclusive)
void flip(int *arr, int i)
{
    int low = 0, high = i;
    while (low < high)
    {
        swap(&arr[low], &arr[high]);
        low++;
        high--;
    }
}

// RETURN INDEX OF MAX ELEMENT IN THE ARRAY
int findMax(int *arr, int n)
{
    int maxIndx = 0;
    
    for(int i = 1; i < n; i++)
        if(arr[i] > arr[maxIndx])
            maxIndx = i;
    return maxIndx;
}

void pancakeSort(int *arr, int n)
{
    while(n>1)
    {
        int maxIdx = findMax(arr, n);
        
        // MOVE MAX ELEMENT AT END
        if (maxIdx != n-1)
        {
            // MOVE MAX ELEMENT AT FRONT
            flip(arr, maxIdx);
            
            // MOVE FIRST ELEMENT AT END
            flip(arr, n-1);
        }
        n--;
    }
}

int main()
{
    int arr[] = {2, 4, 6, 3, 9, 1, 5, 7, 0, 8};
    
    printf("ORIGINAL ARRAY : "); printArray(arr, 10);
    pancakeSort(arr, 10);
    printf("\nSORTED ARRAY : "); printArray(arr, 10);
    
    return 0;
}
