//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 11.1

#include <stdio.h>

void printArray(double *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%lf ", arr[i]);
    
    printf("\n");
}

void shiftSort(double *arr, int n, double *range, int k)
{
    int pos = 0;	// POSITION FROM WHERE TO START CURRENT SORTING
    
    // i’th PASS OF LOOP MOVE ELEMENTS EQUAL TO i'th ELEMENT
    for(int i=0; i<k-1; i++)
    {
        int low = pos;
        int high = n-1;
        while(low < high)
        {
            while(arr[low] == range[i] && low < n)
                low++;
            while(arr[high] != range[i] && high >= pos)
                high--;
            if(low < high)
            {
                double temp = arr[low];
                arr[low] = arr[high];
                arr[high] = temp;
            }
        }
        pos = high+1;
    }
}

int main()
{

    double arr[] = {1, 2.5, 1, 3, 1, 5.8, 2.5, 0, 2.5, 5.8};
    double set[] = {0, 1, 2.5, 3, 5.8};

    printf("ORIGINAL ARRAY : "); printArray(arr, 10);
    shiftSort(arr, 10, set, 5);
    printf("SORTED ARRAY : "); printArray(arr, 10);
    
    return 0;
}
