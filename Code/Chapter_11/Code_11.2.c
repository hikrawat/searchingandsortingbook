//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 11.2

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int data;
    struct node* next;
} Node;

Node* addNode(int v)
{
    Node * temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->next = NULL;
    return temp;
}

void printfList(Node* head)
{
    while(head != NULL)
    {
        printf("%d ", head->data);
        head = head->next;
    }
}

void sortUsingcounting(Node* head)
{
    // IF LIST HAS ZERO OR ONE NODE.
    if(head == NULL || head->next == NULL)
        return;
    
    int cnt0=0, cnt1=0, cnt2=0, i=0;
    
    // COUNT OCCURRENCES
    for(Node* temp = head; temp != NULL; temp = temp->next)
    {
        switch(temp->data)
        {
            case 0:
                cnt0++; break;
            case 1:
                cnt1++; break;
            case 2:
                cnt2++; break;
        }
    }
    
    // SETTING FIRST cnt0 ELEMENTS TO 0
    for(i=0; i<cnt0; i++)
    {
        head->data = 0;
        head = head->next;
    }
    
    // SETTING NEXT cnt1 ELEMENTS TO 1
    for(i=0; i<cnt1; i++)
    {
        head->data = 1;
        head = head->next;
    }
    
    // SETTING LAST cn2 NODES TO 2
    for(i=0; i<cnt2; i++)
    {
        head->data = 2; 
        head = head->next;
    }
}

int main()
{
    Node *h = addNode(1);
    h->next = addNode(1);
    h->next->next = addNode(2);
    h->next->next->next = addNode(0);
    h->next->next->next->next = addNode(2);
    h->next->next->next->next->next = addNode(0);
    h->next->next->next->next->next->next = addNode(1);
    h->next->next->next->next->next->next->next = addNode(0);
    
    printf("ORIGINAL LIST : "); printfList(h);
    sortUsingcounting(h);
    printf("\nSORTED LIST   : "); printfList(h);
    
    return 0;
}
