//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 9.6

#include <stdio.h>
#include <stdlib.h>
#define MAX_NODES 32

typedef struct node
{
    int data;
    struct node* left;
    struct node* right;
} Node;

void inOrder(Node * root)
{
    if(root == NULL) { return; } // TERMINATING CONDITION
    
    inOrder(root->left);
    printf("%d ", root->data);
    inOrder(root->right);
}

// TRAVERSE IN INORDER AND STORE NODE VALUES IN ARRAY arr
void populateNodesInArray(Node* r, int* arr, int pos)
{
    if(r == NULL)
        return;
    
    arr[pos] = r->data;
    if(r->left != NULL)
        populateNodesInArray(r->left, arr, 2*pos + 1);
    
    if(r->right != NULL)
        populateNodesInArray(r->right, arr, 2*pos + 2);
}

void treeToArray(Node* root, int* arr, int maxNodes)
{
    // INITIALIZE ALL VALUES IN ARRAY TO -1
    for(int i=0; i<maxNodes; i++)
        arr[i] = -1;
    
    populateNodesInArray(root, arr, 0);
}

Node* createNode(int v)
{
    Node* temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->left = temp->right = NULL;
    return temp;
}

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

int main()
{
    Node* root = createNode(10);
    
    root->left = createNode(5);
    root->right = createNode(30);
    
    root->left->left = createNode(4);
    root->left->right = createNode(8);
    
    root->right->right = createNode(40);
    
    root->left->left->left = createNode(1);
    
    printf("IN-ORDER TRAVERSAL OF TREE :");
    inOrder(root);
    
    int arr[MAX_NODES];
    treeToArray(root, arr, 15);
    
    printf("ARRAY IS :"); printArray(arr, 15);

    
    return 0;
}