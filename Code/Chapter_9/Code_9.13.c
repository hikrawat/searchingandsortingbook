//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 9.13

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

typedef int bool;
#define true 1
#define false 0

#define N 4

// HELPER FUCTIONS - START
typedef struct MinHeapNode
{
    int data;
    int x;    // ARRAY THAT THIS ELEMENT BELONGS TO
    int y;    // INDEX OF NEXT ELEMENET TO BE PICKED
}MinHeapNode;

void swap(MinHeapNode *a, MinHeapNode *b)
{
    MinHeapNode temp = *a;
    *a = *b;
    *b = temp;
}

void heapify(MinHeapNode *arr, int n, int root)
{
    if(n<=0){ return; }
    if(root <= n/2)    // LEAF NODES ARE ALREADY HEAPIFIED
    {
        int min=root, left = 2*root + 1, right = 2*root + 2;
        
        if(left<n && arr[left].data < arr[min].data)
            min = left;    // LEFT CHILD IS MAXIMUM
        
        if(right < n && arr[right].data < arr[min].data)
            min = right;   // RIGHT CHILD IS MAXIMUM
        
        if(min != root)
        {
            swap(&arr[root], &arr[min]);
            heapify(arr, n, min);
        }
    }
}

void buildHeap(MinHeapNode *arr, int n)
{
    for(int i=n/2; i>=0; i--)
        heapify(arr, n, i);
}

void printMerged(int arr[][N], int m)
{
    // CREATING HEAP
    MinHeapNode *heap = (MinHeapNode*)malloc(m * sizeof(MinHeapNode));
    
    // STORE FIRST ELEMENT OF EACH ARRAY IN HEAP
    for(int i=0; i<m; i++)
    {
        heap[i].data = arr[i][0];
        heap[i].x = i; heap[i].y = 1;
    }
    
    buildHeap(heap, m);
    
    for (int cnt = 0; cnt < N*m; cnt++)
    {
        MinHeapNode root = heap[0];
        printf("%d ", root.data);
        
        if(heap[0].y < N)
        {
            heap[0].data = arr[root.x][root.y];
            heap[0].y++;
        }
        else
        {
            heap[0].data = INT_MAX;
        }
        heapify(heap, m, 0);
    }
    free(heap);
}

int main()
{
    int arr[3][4] = { {1, 6, 12, 20},
                    {3, 5,  7,  9},
                    {2, 8, 25, 49} };

    printMerged(arr, 3);
    
    return 0;
}
