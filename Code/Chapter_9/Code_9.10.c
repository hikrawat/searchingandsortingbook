//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 9.10

#include <stdio.h>

typedef int bool;
#define true 1
#define false 0

// HELPER FUCTIONS - START
void swap(int* a, int*b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

// r – ROOT INDEX. arr - HEAP ARRAY
bool isHeap(int *arr, int n, int r)
{
    if(r > n/2)      // LEAF NODE
        return true;
    
    bool retValue = true;
    
    // LEFT SUBTREE VOILATE HEAP
    if(arr[r] < arr[2*r+1] || !isHeap(arr, n, 2*r+1))
        retValue = false;
    
    // RIGHT SUBTREE EXIST AND VOILATE HEAP
    if(2*r+2 < n && (arr[r] < arr[2*r+2] || !isHeap(arr, n, 2*r+2)))
        retValue = false;
    
    return retValue;
}

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}
// HELPER FUNCTIONS - END

void heapify(int *arr, int n, int root)
{
    if(n<=0){ return; }
    
    if(root < n/2)      // LEAF NODES ARE ALREADY HEAPIFIED
    {
        int max=root, left = 2*root + 1, right = 2*root + 2;
        
        if(left<n && arr[left] > arr[max])
            max = left;    // LEFT CHILD IS MAXIMUM
        
        if(right < n && arr[right] > arr[max])
            max = right;    // RIGHT CHILD IS MAXIMUM
        
        if(max != root)
        {
            swap(&arr[root], &arr[max]);
            heapify(arr, n, max);
        }
    }
}

void buildHeap(int *arr, int n)
{
    for(int i=n/2; i>=0; i--)
        heapify(arr, n, i);
}

int main()
{
    int arr[] = {11, 4, 10, 14, 7, 9, 3, 2, 8, 1};
    
    printf("ORIGINAL ARRAY : "); printArray(arr, 10);

    buildHeap(arr,10);
    printf("HEAP ARRAY : "); printArray(arr, 10);
    
    if(isHeap(arr, 10, 0))
        printf("IS HEAP");
    else
        printf("IS NOT HEAP");
    return 0;
}