//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 9.7

#include <stdio.h>
#include <stdlib.h>
#define MAX_NODES 32

typedef struct node
{
    int data;
    struct node* left;
    struct node* right;
} Node;

Node* createNode(int v)
{
    Node* temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->left = temp->right = NULL;
    return temp;
}

void inOrder(Node * root)
{
    if(root == NULL) { return; } // TERMINATING CONDITION
    
    inOrder(root->left);
    printf("%d ", root->data);
    inOrder(root->right);
}

// pos IS POSITION OF CURRENT ROOT IN THE ARRAY
void populateTreeFromArray(Node* r, int* arr, int n, int pos)
{
    if(r == NULL || arr == NULL || n==0)
        return;
    
    // SETTING LEFT SUBTREE
    int newPos = 2*pos+1;
    if(newPos < n && arr[newPos] != -1)
    {
        r->left = createNode(arr[newPos]);  // CHANGE IT TO malloc FOR C LANGUAGE
        populateTreeFromArray(r->left, arr, n, newPos);
    }
    
    // SETTING RIGHT SUBTREE
    newPos = 2*pos+2;
    if(newPos < n && arr[newPos] != -1)
    {
        r->right = createNode(arr[newPos]);  // CHANGE IT TO malloc FOR C LANGUAGE
        populateTreeFromArray(r->right, arr, n, newPos);
    }
}

Node* arrayToTree(int* arr, int n)
{
    // TERMINATING CONDITION
    if(arr == NULL || arr[0] == -1)
        return NULL;
    
    // POPULATE THE ROOT HERE
    // REST OF THE TREE WILL BE POPULATED BY FUNCTION populateTreeFromArray
    Node* root = createNode(arr[0]);  // CHANGE IT TO malloc FOR C LANGUAGE
    populateTreeFromArray(root, arr, n, 0);
    
    return root;
}

// TRAVERSE IN INORDER AND STORE NODE VALUES IN ARRAY arr
void populateNodesInArray(Node* r, int* arr, int pos)
{
    if(r == NULL)
        return;
    
    arr[pos] = r->data;
    if(r->left != NULL)
        populateNodesInArray(r->left, arr, 2*pos + 1);
    
    if(r->right != NULL)
        populateNodesInArray(r->right, arr, 2*pos + 2);
}

void treeToArray(Node* root, int* arr, int maxNodes)
{
    // INITIALIZE ALL VALUES IN ARRAY TO -1
    for(int i=0; i<maxNodes; i++)
        arr[i] = -1;
    
    populateNodesInArray(root, arr, 0);
}

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

int main()
{
    Node* root = createNode(10);
    
    root->left = createNode(5);
    root->right = createNode(30);
    
    root->left->left = createNode(4);
    root->left->right = createNode(8);
    
    root->right->right = createNode(40);
    
    root->left->left->left = createNode(1);
    
    printf("IN-ORDER TRAVERSAL OF TREE :");
    inOrder(root);
    
    int arr[MAX_NODES];
    treeToArray(root, arr, 15);
    
    printf("\nARRAY IS :"); printArray(arr, 15);
    
    Node* newRoot = arrayToTree(arr, 15);
    printf("\nIN-ORDER TRAVERSAL OF NEW TREE :");
    inOrder(newRoot);

    
    return 0;
}