//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 9.11

#include <stdio.h>

typedef int bool;
#define true 1
#define false 0

// HELPER FUCTIONS - START
void swap(int* a, int*b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}
// HELPER FUNCTIONS - END
void insertInHeap(int *arr, int n, int value)
{
    arr[n] = value; // APPEND ELEMENT
    while(n > 0)
    {
        int parentIndx = (n-1)/2; // INDEX OF PARENT NODE
        
        if(arr[parentIndx] < arr[n])
            swap(&arr[parentIndx], &arr[n]);
        else
            break;                  // STOP WHEN ROOT IS LARGER
        
        n = parentIndx;
    }
}

int main()
{
    int arr[11] = {14, 11, 10, 8, 7, 9, 3, 2, 4, 1};
    
    printf("ORIGINAL ARRAY : "); printArray(arr, 10);

    insertInHeap(arr, 10, 15);
    printf("HEAP ARRAY : "); printArray(arr, 11);
    
    return 0;
}
