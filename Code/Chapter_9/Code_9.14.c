//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 9.14

#include <stdio.h>

typedef int bool;
#define true 1
#define false 0

// HELPER FUCTIONS - START
struct MinHeapNode
{
    int data;
    int x;    // ARRAY THAT THIS ELEMENT BELONGS TO
    int y;    // INDEX OF NEXT ELEMENET TO BE PICKED
};

void swap(int* a, int*b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

void heapify(int *arr, int n, int root)
{
    if(n<=0){ return; }
    
    if(root < n/2)      // LEAF NODES ARE ALREADY HEAPIFIED
    {
        int max=root, left = 2*root + 1, right = 2*root + 2;
        
        if(left<n && arr[left] > arr[max])
            max = left;    // LEFT CHILD IS MAXIMUM
        
        if(right < n && arr[right] > arr[max])
            max = right;    // RIGHT CHILD IS MAXIMUM
        
        if(max != root)
        {
            swap(&arr[root], &arr[max]);
            heapify(arr, n, max);
        }
    }
}

void buildHeap(int *arr, int n)
{
    for(int i=n/2; i>=0; i--)
        heapify(arr, n, i);
}

// HELPER FUNCTIONS - END

void heapSort(int *arr, int n)
{
    // Convert the array to heap.
    buildHeap(arr, n);
    
    for(int i = n-1; i >= 0; i--)
    {
        // SWAP FIRST AND LAST ELEMENT
        swap(&arr[0], &arr[i]);
        
        // SIZE OF HEAP IS i
        heapify(arr, i, 0);
    }
}

int main()
{
    int arr[] = {18, 7, 56, 9, 6, 4, 8, 13, 12, 5};
    
    printf("ORIGINAL ARRAY : "); printArray(arr, 10);
    heapSort(arr, 10);
    printf("SORTED ARRAY : "); printArray(arr, 10);
    
    return 0;
}