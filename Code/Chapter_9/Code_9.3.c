//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 9.3

#include <stdio.h>

char getParent(int *arr, int n, int i)
{
    if(i == 0)
        return '\0';     // ROOT DOES NOT HAVE A PARENT
    else
        return arr[(i-1)/2];
};
