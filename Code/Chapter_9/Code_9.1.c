//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 9.1

#include <stdio.h>
#include <stdlib.h>

#define true 1
#define false 0
typedef int bool;

// HELPER FUNCTIONS - START
typedef struct node
{
    int data;
    struct node* left;
    struct node* right;
} Node;

Node* createNode(int v)
{
    Node* temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->left = temp->right = NULL;
    return temp;
}

void inOrder(Node * root)
{
    if(root == NULL) { return; } // TERMINATING CONDITION
    
    inOrder(root->left);
    printf("%d ", root->data);
    inOrder(root->right);
}
// HELPER FUNCTION - END

bool isStrictly(Node* r)
{
    // NULL OR LEAF NODE
    if(r == NULL || (r->left == NULL && r->right == NULL) )
        return true;
    
    if(r->left == NULL || r->right == NULL)
        return false;
    else
        return isStrictly(r->left) && isStrictly(r->right);
}

int main()
{
    Node* root = createNode(10);
    
    root->left = createNode(5);
    root->right = createNode(15);
    
    root->left->left = createNode(2);
    root->left->right = createNode(8);
    
    root->right->right = createNode(19);
    
    root->left->right->left = createNode(6);
    
    printf("IN-ORDER TRAVERSAL OF TREE :");
    inOrder(root);
    
    if(isStrictly(root))
        printf("STRICTLY BINARY TREE");
    else
        printf("NOT STRICTLY BINARY TREE");
    
    return 0;
}
