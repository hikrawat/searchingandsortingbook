//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 9.12

#include <stdio.h>

typedef int bool;
#define true 1
#define false 0

// HELPER FUCTIONS - START
void swap(int* a, int*b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

void heapify(int *arr, int n, int root)
{
    if(n<=0){ return; }
    
    if(root < n/2)      // LEAF NODES ARE ALREADY HEAPIFIED
    {
        int max=root, left = 2*root + 1, right = 2*root + 2;
        
        if(left<n && arr[left] > arr[max])
            max = left;    // LEFT CHILD IS MAXIMUM
        
        if(right < n && arr[right] > arr[max])
            max = right;    // RIGHT CHILD IS MAXIMUM
        
        if(max != root)
        {
            swap(&arr[root], &arr[max]);
            heapify(arr, n, max);
        }
    }
}

// HELPER FUNCTIONS - END

// SIZE OF HEAP WILL BE DECREASED BY 1. REMOVED ELEMENT IS PUT AT arr[n-1].
void removeFromHeap(int *arr, int n)
{
    if(n<=0)
        return;
    
    // SWAP FIRST AND LAST VALUE
    swap(&arr[0], &arr[n-1]);
    
    heapify(arr, n-1, 0);
}

int main()
{
    int arr[] = {14, 11, 10, 8, 7, 9, 3, 2, 4, 1};
    
    printf("ORIGINAL ARRAY : "); printArray(arr, 10);

    removeFromHeap(arr, 10);
    printf("HEAP ARRAY : "); printArray(arr, 9);
    
    return 0;
}
