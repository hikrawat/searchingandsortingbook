//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 9.5

#include <stdio.h>
#include <stdlib.h>

void inOrder(int root, char* arr, int n)
{
    if(root >= n){ return; }     // TERMINATING CONDITION
    
    inOrder(2*root+1, arr, n);     // TRAVERSE LEFT
    printf("%c ", arr[root]);      // PRINT DATA
    inOrder(2*root+2, arr, n);     // TRAVERSE RIGHT
}


int main()
{
    char tree[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};
    
    printf("IN-ORDER TRAVERSAL OF TREE :");
    inOrder(0, tree, 7);
    
    return 0;
}