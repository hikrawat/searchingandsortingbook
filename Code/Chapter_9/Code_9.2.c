//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 9.2

#include <stdio.h>
#include <stdlib.h>

#define true 1
#define false 0
typedef int bool;

// HELPER FUNCTIONS - START
typedef struct node
{
    int data;
    struct node* left;
    struct node* right;
} Node;

Node* createNode(int v)
{
    Node* temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->left = temp->right = NULL;
    return temp;
}

void inOrder(Node * root)
{
    if(root == NULL) { return; } // TERMINATING CONDITION
    
    inOrder(root->left);
    printf("%d ", root->data);
    inOrder(root->right);
}
// HELPER FUNCTION - END

bool leafAtSameLevelRec(Node* r, int currentLevel, int* leafLevel)
{
    // TERMINATING CONDITION
    if(r == NULL)
        return true;

    // LEAF NODE
    if(r->left == NULL && r->right == NULL)
    {
        // FIRST LEAF NODE FOUND
        if(*leafLevel == -1)
            *leafLevel = currentLevel;
        else if(currentLevel == *leafLevel)
            return true;
        else
            return false;
    }
    
    return leafAtSameLevelRec(r->left, currentLevel+1, leafLevel)
    && leafAtSameLevelRec(r->right, currentLevel+1, leafLevel);
}

bool leafAtSameLevel(Node* r)
{
    int leafLevel = -1;       // NO LEAF LEVEL
    return leafAtSameLevelRec(r, 0, &leafLevel);
}

int main()
{
    Node* root = createNode(10);
    
    root->left = createNode(5);
    root->right = createNode(15);
    
    root->left->left = createNode(2);
    root->left->right = createNode(8);
    
    root->right->right = createNode(19);
    
    root->left->right->left = createNode(6);
    
    printf("IN-ORDER TRAVERSAL OF TREE :");
    inOrder(root);
    
    if(leafAtSameLevel(root))
        printf("\nLEAVES AT SAME LEVEL");
    else
        printf("\nLEAVES NOT AT THE SAME LEVEL");
    
    return 0;
}
