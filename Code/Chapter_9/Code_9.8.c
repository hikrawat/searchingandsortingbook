//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 9.8

#include <stdio.h>

typedef int bool;
#define true 1
#define false 0

// r – ROOT INDEX. arr - HEAP ARRAY
bool isHeap(int *arr, int n, int r)
{
    if(r > n/2)      // LEAF NODE
        return true;

    bool retValue = true;
    
    // LEFT SUBTREE VOILATE HEAP
    if(arr[r] < arr[2*r+1] || !isHeap(arr, n, 2*r+1))
        retValue = false;
    
    // RIGHT SUBTREE EXIST AND VOILATE HEAP
    if(2*r+2 < n && (arr[r] < arr[2*r+2] || !isHeap(arr, n, 2*r+2)))
        retValue = false;
    
    return retValue;
}

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

int main()
{
    int arr1[] = {15, 19, 10, 7, 17, 16};
    int arr2[] = {15, 12, 10, 7, 11, 8};
    
    printf("ARRAY : "); printArray(arr1, 6);
    if(isHeap(arr1, 6, 0))
        printf("IS HEAP");
    else
        printf("IS NOT HEAP");

    printf("\nARRAY : "); printArray(arr2, 6);
    if(isHeap(arr2, 6, 0))
        printf("IS HEAP");
    else
        printf("IS NOT HEAP");
    
    return 0;
}