//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 9.4

#include <stdio.h>
#include <stdlib.h>

#define true 1
#define false 0
typedef int bool;

typedef struct node
{
    char data;
    struct node* left;
    struct node* right;
} Node;


void inOrder(Node* root)
{
    if(root == NULL){ return; } // TERMINATING CONDITION
    
    inOrder(root->left);        // TRAVERSE LEFT
    printf("%c ", root->data);  // PRINT DATA
    inOrder(root->right);       // TRAVERSE RIGHT
}

Node* createNode(char v)
{
    Node* temp = (Node*)malloc(sizeof(Node));
    temp->data = v;
    temp->left = temp->right = NULL;
    return temp;
}

int main()
{
    Node* root = createNode('A');
    
    root->left = createNode('B');
    root->right = createNode('C');
    
    root->left->left = createNode('D');
    root->left->right = createNode('E');
    
    root->right->left = createNode('F');
    root->right->right = createNode('G');
    
    printf("IN-ORDER TRAVERSAL OF TREE :");
    inOrder(root);
    
    return 0;
}
