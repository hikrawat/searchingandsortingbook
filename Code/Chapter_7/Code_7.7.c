//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 7.7

#include <stdio.h>

//HELPER FUNCTIONS - START
void swap(int* a, int*b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}
// HELPER FUNCTIONS - END

int partition(int *arr, int low, int high)
{
    int p = low; // Pivot index
    while(low < high)
    {
        while(low <= high && arr[low]<=arr[p]) // MOVING LOW FORWARD
            low++;
        while(low <= high && arr[high]>arr[p])  // MOVING HIGH BACKWARD
            high--;
        if(low < high)
            swap(&arr[low], &arr[high]);
    }
    swap(&arr[p], &arr[high]);
    return high;
}

int kthSmallest(int arr[], int l, int h, int k)
{
    // IF k IS MORE THAN NUMBER OF ELEMENTS IN ARRAY
    if(k>h)
        return -1;
    int m = partition(arr, l, h);
    
    // PIVOT IS AT k
    if(m == k-1)
        return arr[m];
    
    // PIVOT IS AT RIGHT OF k, RECURSE ON LEFT SIDE
    if(m > k-1)
        return kthSmallest(arr, l, m, k);
    
    // RECURSE ON RIGHT SIDE
    return kthSmallest(arr, m+1, h, k);
}

int main()
{
    int arr[] = {9, 0, 1, 6, 3, 2, 4, 7, 5, 8};
    
    printf("k'th Smallest : %d", kthSmallest(arr, 0, 9, 7));
    
    return 0;
}
