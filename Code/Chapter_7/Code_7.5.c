//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 7.5

#include <stdio.h>

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

void sortBinaryArray(int *arr, int n)
{
    int cntZero = 0, i;
    
    // COUNT ZEROS IN THE ARRAY
    for(i=0; i<n ;i++)
        if(arr[i] == 0)
            cntZero++;
    
    // MAKE FIRST cntZero POSITIONS 0
    for(i=0; i<cntZero; i++)
        arr[i] = 0;
    
    // ALL OTHER ELEMENTS ARE 1
    for(i=cntZero; i<n; i++)
        arr[i] = 1;
}

int main()
{
    int arr[] = {0, 1, 0, 1, 0, 0, 1, 0, 1, 0};
    
    printf("ORIGINAL ARRAY :"); printArray(arr, 10);
    sortBinaryArray(arr, 10);
    printf("SORTED ARRAY   :"); printArray(arr, 10);
    
    return 0;
}