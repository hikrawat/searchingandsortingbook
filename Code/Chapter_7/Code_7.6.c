//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 7.6

#include <stdio.h>

void swap(int* a, int*b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}

void sortBinaryArray2(int *arr, int n)
{
    int low = 0, high = n-1;
    
    while(low<high)
    {
        while(low<high && arr[low] == 0)
            low++;
        while(low<high && arr[high] == 1)
            high--;
        if(low<high)
            swap(&arr[low], &arr[high]);
    }
}

int main()
{
    int arr[] = {0, 1, 0, 1, 0, 0, 1, 0, 1, 0};
    
    printf("ORIGINAL ARRAY :"); printArray(arr, 10);
    sortBinaryArray2(arr, 10);
    printf("SORTED ARRAY   :"); printArray(arr, 10);
    
    return 0;
}
