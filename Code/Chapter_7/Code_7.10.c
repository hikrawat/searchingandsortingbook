//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 7.10

#include <stdio.h>

// HELPER FUNCTIONS - START
void swap(int* a, int*b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}
void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}
// HELPER FUNCTIONS - END

void partition(int* a, int low, int high, int *i, int *j)
{
    // If there are <= 2 elements
    if(low+1 >= high)
    {
        if (a[high] < a[low])
            swap(&a[high], &a[low]);
        *i = low;
        *j = high;
        return;
    }
    
    int m = low;
    int pivot = a[high];
    
    while(m <= high)
    {
        if(a[m] < pivot)
        {  swap(&a[low], &a[m]); low++; m++; }
        else if(a[m] == pivot)
            m++;
        else if(a[m] > pivot)
        {  swap(&a[m], &a[high]); high--; }
    }
    
    //setting return values
    *i = low - 1;
    *j = m; //or high-1
}

void quicksort(int a[], int low, int high)
{
    if (low >= high)
        return;
    
    int i, j;
    
    partition(a, low, high, &i, &j);
    
    quicksort(a, low, i);
    quicksort(a, j, high);
}

int main()
{
    int arr[] = {9, 0, 1, 6, 3, 2, 4, 7, 5, 8};
    
    printf("ORIGINAL ARRAY :"); printArray(arr, 10);
    quicksort(arr, 0, 9);
    printf("SORTED ARRAY   :"); printArray(arr, 10);
    
    return 0;
}

