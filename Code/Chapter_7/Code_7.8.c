//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 7.8

#include <stdio.h>

int minMergeToPalindrome(int *arr, int n)
{
    int count = 0;
    int low = 0, high = n-1;

    while(low<=high)
    {
        if(arr[low] == arr[high])
        {
            low++; high--;
            continue;       // NOT MERGING REQUIRED
        }

        if(arr[low] > arr[high])
        {
            arr[high-1] += arr[high];
            high--;
        }
        else
        {
            arr[low+1] += arr[low];
            low++;
        }
        count++;
    }
    return count;
}


int main()
{
    int arr[] = {7, 8, 3, 4};
    
    printf("Min Merge to Palindrome : %d", minMergeToPalindrome(arr, 4));
    
    return 0;
}

