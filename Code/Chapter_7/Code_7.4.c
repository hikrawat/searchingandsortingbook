//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 7.4

#include <stdio.h>

//HELPER FUNCTIONS - START
void swap(int* a, int*b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}
// HELPER FUNCTIONS - END

int partition(int *arr, int low, int high)
{
    int p = low; // Pivot index
    while(low < high)
    {
        while(low <= high && arr[low]<=arr[p]) // MOVING LOW FORWARD
            low++;
        while(low <= high && arr[high]>arr[p])  // MOVING HIGH BACKWARD
            high--;
        if(low < high)
            swap(&arr[low], &arr[high]);
    }
    swap(&arr[p], &arr[high]);
    return high;
}

void quickSort(int arr[], int l, int h)
{
    while (l < h)
    {
        int m = partition(arr, l, h);
        quickSort(arr, l, h - 1);
        l = m+1;
    }
}

int main()
{
    int arr[] = {5, 0, 1, 6, 3, 2, 4, 7, 9, 8};
    
    printf("ORIGINAL ARRAY :"); printArray(arr, 10);
    quickSort(arr, 0, 9);
    printf("SORTED ARRAY   :"); printArray(arr, 10);
    
    return 0;
}
