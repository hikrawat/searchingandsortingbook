//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 7.1

#include <stdio.h>

//HELPER FUNCTIONS - START
void swap(int* a, int*b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}
// HELPER FUNCTIONS - END

int partition(int *arr, int low, int high)
{
    int p = low; // Pivot index
    while(low < high)
    {
        while(low <= high && arr[low]<=arr[p]) // MOVING LOW FORWARD
            low++;
        while(low <= high && arr[high]>arr[p])  // MOVING HIGH BACKWARD
            high--;
        if(low < high)
            swap(&arr[low], &arr[high]);
    }
    swap(&arr[p], &arr[high]);
    return high;
}

int main()
{
    int arr[] = {5, 0, 1, 6, 3, 2, 4, 7, 9, 8};
    
    printf("ORIGINAL ARRAY  :"); printArray(arr, 10);
    partition(arr, 0, 9);
    printf("AFTER PARTITION :"); printArray(arr, 10);
    
    return 0;
}
