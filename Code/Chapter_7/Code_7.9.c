//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 7.9

#include <stdio.h>

void partition(int* a, int low, int high, int *i, int *j)
{
    // If there are <= 2 elements
    if(low+1 >= high)
    {
        if (a[high] < a[low])
            swap(&a[high], &a[low]);
        *i = low;
        *j = high;
        return;
    }
    
    int m = low;
    int pivot = a[high];
    
    while(m <= high)
    {
        if(a[m] < pivot)
        {  swap(&a[low], &a[m]); low++; m++; }
        else if(a[m] == pivot)
            m++;
        else if(a[m] > pivot)
        {  swap(&a[m], &a[high]); high--; }
    }
    
    //setting return values
    *i = low - 1;
    *j = m; //or high-1
}

int main()
{
    return 0;
}