//  Created by Kamal Rawat for Ritambhara Technologies.
//  Copyright © 2017 Ritambhara Technologies. All rights reserved.

//  Code 7.2

#include <stdio.h>

//HELPER FUNCTIONS - START
void swap(int* a, int*b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void printArray(int *arr, int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
}
// HELPER FUNCTIONS - END

int partition (int *arr, int low, int high)
{
    int pivot = arr[high];
    int i = (low - 1);   // INDEX OF LAST ELEMENT < pivot
    
    for(int j = low; j <= high- 1; j++)
    {
        // IF CURRENT ELEMENT <= pivot, PLACE IT AT (i+1) POSITION.
        if (arr[j] <= pivot)
        {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i+1], &arr[high]);
    return (i+1);
}

int main()
{
    int arr[] = {5, 0, 1, 6, 3, 2, 8, 7, 9, 4};
    
    printf("ORIGINAL ARRAY  :"); printArray(arr, 10);
    partition(arr, 0, 9);
    printf("AFTER PARTITION :"); printArray(arr, 10);
    
    return 0;
}