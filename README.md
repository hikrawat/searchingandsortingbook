# README #

At Ritambhara (www.ritambhara.in), we discuss the topics in computer software exhaustively from interview point of view. 
There are different types of problem solving questions that can be asked in test and interview of IT companies.

Idea is to clear the concept, have in-depth knowledge of subject and know multiple ways to tackle different problem solving questions.

### What is this repository for? ###

This repository contains code of Examples and questions given in book, "Searching & Sorting for Coding Interviews". 
You can buy the book online at https://www.amazon.in/Kamal-Rawat/e/B0765VFLKD/ref=dp_byline_cont_book_2

### How do I get set up? ###


### Who do I talk to? ###

For any querry/suggestion/feedback contact us at hikrawat@gmail.com or call me @ +91-8377803450.